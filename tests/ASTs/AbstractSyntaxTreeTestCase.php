<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\ASTs;

use judahnator\Lua\AST\AST;
use judahnator\Lua\AST\Compiler;
use Judahnator\Lexer\Buffer\CharacterBuffer as Characters;
use judahnator\Lua\Lexer;
use PHPUnit\Framework\TestCase;

abstract class AbstractSyntaxTreeTestCase extends TestCase
{
    /**
     * @param string $input
     * @return \Generator<AST>
     */
    protected static function express(string $input): \Generator
    {
        return (new Compiler())->compile(
            (new Lexer())->tokenize(new Characters($input))
        );
    }

    abstract public function provideCountData(): array;

    /**
     * @dataProvider provideCountData
     */
    final public function testCountFunction(string $expression, int $expectedCount): void
    {
        $this->assertCount($expectedCount, self::express($expression)->current());
    }

    abstract public function provideStringableData(): array;

    /**
     * @dataProvider provideStringableData
     */
    final public function testStringableInterface(string $inputAndExpected): void
    {
        $this->assertEquals($inputAndExpected, (string)self::express($inputAndExpected)->current());
    }

    abstract public function provideJsonSerializeData(): array;

    /**
     * @dataProvider provideJsonSerializeData
     */
    final public function testJsonSerializeFunction(string $expression, string $expected): void
    {
        $this->assertEquals(
            $expected,
            json_encode(self::express($expression)->current(), JSON_PRETTY_PRINT)
        );
    }
}
