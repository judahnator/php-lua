<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\Tokens;

use JetBrains\PhpStorm\ArrayShape;
use Judahnator\Lexer\Buffer\CharacterBuffer as Characters;;
use Judahnator\Lexer\TokenMismatchException;
use judahnator\Lua\Lexer;
use judahnator\Lua\Lexer\Tokens\Token;
use PHPUnit\Framework\TestCase;

abstract class TokenTestCase extends TestCase
{
    #[ArrayShape([["callable", TokenMismatchException::class]])]
    abstract public function provideConstructionExceptionData(): array;

    abstract public function provideTokenData(): array;

    /**
     * @dataProvider provideConstructionExceptionData
     * @param callable $throws
     * @param TokenMismatchException $expected
     */
    final public function testConstructionException(callable $throws, TokenMismatchException $expected): void
    {
        $this->expectExceptionObject($expected);
        $throws();
    }

    /**
     * @dataProvider provideTokenData
     * @param string $tokenString
     * @param string $name
     * @param int $count
     * @param string $literal
     */
    final public function testTokenData(string $tokenString, string $name, int $count, string $literal): void
    {
        $token = (new Lexer())->tokenize(new Characters($tokenString))->readOne();
        $this->assertEquals($name, $token->getName());
        $this->assertCount($count, $token);
        $this->assertEquals($literal, $token->getLiteral());
    }
}