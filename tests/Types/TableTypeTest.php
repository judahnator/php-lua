<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\Types;

use InvalidArgumentException;
use judahnator\Lua\Types\NilType;
use judahnator\Lua\Types\NumberType;
use judahnator\Lua\Types\StringType;
use judahnator\Lua\Types\TableType;

/**
 * Class TableTypeTest
 * @package judahnator\Lua\Tests\Types
 * @covers \judahnator\Lua\Types\TableType
 * @uses \judahnator\Lua\Types\StringType
 */
final class TableTypeTest extends TypeTestCase
{
    public function provideBooleanData(): array
    {
        return [
            'all tables are true' => [
                new TableType([]), true,
            ],
        ];
    }

    public function provideStringableData(): array
    {
        return [
            'empty tables are empty' => [
                '{}', new TableType([]),
            ],
            'maps are mapped' => [
                '{foo: "bar"}', new TableType([
                    'foo' => new StringType("bar"),
                ])
            ],
        ];
    }

    public function provideValueData(): array
    {
        $basic = [
            'foo' => new StringType('bar'),
        ];
        return [
            'empty list looks like empty list' => [
                new TableType([]), [],
            ],
            'basic list looks like basic list' => [
                new TableType($basic), $basic,
            ],
        ];
    }
}