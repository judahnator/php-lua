# The Changelog

## v0.4.0

The "Tables" release.

Tables used to be a construct of the Lexer, which meant they could only be statically initialized with literal values. This release moves the table logic into the AST namespace, so tables can be constructed with any expression. Lists have also been implemented.

```
myTable = {
  foo: "bar"
  bing: 40 + 2
}
myList = [
  "asdf"
  meaningOfLife()
]
```

## v0.3.0

The "Signatures" release. 

The Lexer logic has been delegated to a new `judahnator/lexer` package. It also splits the AST models into separate nodes and node identifiers. 

This is being called the "signatures" release because of the new signature matching done in the AST identifiers. 

## v0.2.0

This release adds support for parenthesis expressions. This allows you to wrap conditionals with parenthesis for easier reading and tweaking formula order of operations.

## v0.1.0

This marks the first "official" release. This is the point where I first started wanting to keep track of the major changes, but it's still definitely not ready for any real use.

Regardless, here is a quick overview of the features so far:

### Expressions

 * Function Applications
 * Variable Assignment
 * Functions (including first-class functions!)
 * Literals (like strings, numbers, tables, etc)
 * Offsets `foo[bar]`
 * Operators (respects precedence)
   * Supported operators: +, -, *, / , % , ^ , ==, ~=, >, >=, <, <=
 * Returns
 * Variables

### Statements

 * Conditionals (if)
 * While Loops

### Standard Library

 * abs - Finds 
 * max
 * min
 * print

### Debugging

There is an inbuilt debugger class that will convert an input string into something you can use to troubleshoot.

`\judahnator\Lua\Debug::ast` will print the JSON-encoded AST tree for the given input, `\judahnator\Lua\Debug::lexer` will print the parsed lexer tokens.