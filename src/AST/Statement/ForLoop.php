<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Statement;

use Judahnator\Lexer\Buffer\TokenBuffer;
use Judahnator\Lexer\Token\Token;
use judahnator\Lua\AST\AST;
use judahnator\Lua\AST\Compiler;
use judahnator\Lua\AST\Expression\AssignmentExpression;
use judahnator\Lua\AST\Expression\Expression;
use judahnator\Lua\Environment;
use judahnator\Lua\Exceptions\AST\MismatchException;
use judahnator\Lua\Lexer\Keywords;
use judahnator\Lua\Operators\Operator;

final class ForLoop extends Statement
{
    /**
     * @param AssignmentExpression $init
     * @param Expression $minmax
     * @param Expression $increment
     * @param AST[] $statements
     */
    public function __construct(
        private AssignmentExpression $init,
        private Expression $minmax,
        private Expression $increment,
        private array $statements,
    ) {}

    /*
    for init,max/min value, increment
    do
       statement(s)
    end
     */
    public function __debugInfo(): array
    {
        return [
            'init' => $this->init,
            'minmax' => $this->minmax,
            'increment' => $this->increment,
            'statements' => $this->statements,
        ];
    }

    public function __toString(): string
    {
        return sprintf(
            "for %s, %s, %s do %s end",
            $this->init,
            $this->minmax,
            $this->increment,
            implode(' ', $this->statements),
        );
    }

    public function count(): int
    {
        return self::countList(
            $this->init,
            $this->minmax,
            $this->increment,
            ...$this->statements,
        ) + 5;
    }

    public function parse(Environment $variables): iterable
    {
        $initial = $this->init->getResult($variables);
        $initialValue = $initial->getValue();
        $minmax = $this->minmax->getResult($variables)->getValue();

        for(
            $this->init->variable->writeValue($variables, $initial);
            self::compareValueForLoop($initialValue, $minmax, $this->init->variable->getResult($variables)->getValue());
            $this->init->variable->writeValue(
                $variables,
                Operator::get('+')->parse(
                    $this->init->variable->getResult($variables),
                    $this->increment->getResult($variables),
                )
            )
        ) {
            yield from $this->statements;
        }
    }

    /**
     * It appears that the comparison is context dependent on the comparison between the initial and minmax values.
     * This does the proper comparison considering the context.
     *
     * @param float $initial
     * @param float $minmax
     * @param float $compare
     * @return bool
     */
    private static function compareValueForLoop(float $initial, float $minmax, float $compare): bool {
        return match ($initial <=> $minmax) {
            -1 => $compare <= $minmax,
            0  => $compare === $minmax,
            1  => $compare >= $minmax,
        };
    }
}