<?php declare(strict_types=1);

namespace judahnator\Lua\Types;

final class NilType implements Type
{
    public function __toString(): string
    {
        return 'nil';
    }

    public function getValue()
    {
        return null;
    }

    public function toBool(): bool
    {
        return false;
    }
}