<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\STD;

use judahnator\Lua\Environment;
use judahnator\Lua\Exceptions\MethodSignatureException;
use judahnator\Lua\STD\Library\PrintFunction;
use judahnator\Lua\STDIO;
use judahnator\Lua\Types\NumberType;
use judahnator\Lua\Types\StringType;

/**
 * @covers \judahnator\Lua\STD\Library\PrintFunction
 * @covers \judahnator\Lua\STD\Library\Method
 */
final class PrintFunctionTest extends MethodTestCase
{
    protected string $methodClass = PrintFunction::class;

    public function provideConstructionErrorsData(): array
    {
        return [
            [new MethodSignatureException('The "print" function requires at least one parameter.')],
        ];
    }

    public function provideInvocationData(): array
    {
        return [
            [null, new StringType('foo')],
            [null, new NumberType(42)],
        ];
    }

    public function testStdio(): void
    {
        $method = new PrintFunction(new Environment(IO: $io = new STDIO(STDIO::DRIVER_MEMORY)));
        $method(new StringType('foo'), new StringType('bar'), new NumberType(42));
        $this->assertEquals('foobar42', $io->out);
    }
}