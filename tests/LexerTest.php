<?php declare(strict_types=1);

namespace judahnator\Lua\Tests;

use judahnator\Lua\Debug;
use judahnator\Lua\Lexer;
use PHPUnit\Framework\TestCase;

/**
 * Class LexerTest
 * @package judahnator\Lua\Tests
 * @covers \judahnator\Lua\Lexer
 */
final class LexerTest extends TestCase
{
    public function testLexer(): void
    {
        $lua = <<<LUA
            hello = 'Hello World!'
            a = 12
            b = a + 34.56
            print(b)
        LUA;
        $expected = <<<EXPECTED
            IDENT, hello
            ASSIGN, =
            STRING, 'Hello World!'
            IDENT, a
            ASSIGN, =
            INT, 12
            IDENT, b
            ASSIGN, =
            IDENT, a
            ADD, +
            FLOAT, 34.56
            IDENT, print
            PAREN_OPEN, (
            IDENT, b
            PAREN_CLOSE, )
            
            EXPECTED;

        $this->assertEquals(
            $expected,
            Debug::lexer($lua),
        );
    }

    public function testIsWhitespaceCharacterFunction(): void
    {
        $this->assertTrue(Lexer::isWhitespaceCharacter(' '));
        $this->assertTrue(Lexer::isWhitespaceCharacter(';'));
        $this->assertTrue(Lexer::isWhitespaceCharacter("\t"));
        $this->assertFalse(Lexer::isWhitespaceCharacter('Ni!'));
    }
}