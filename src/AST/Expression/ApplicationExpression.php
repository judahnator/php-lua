<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Expression;

use JetBrains\PhpStorm\Immutable;
use JetBrains\PhpStorm\Pure;
use judahnator\Lua\Environment;
use judahnator\Lua\Exceptions\AST\RuntimeException;
use judahnator\Lua\Parser;
use judahnator\Lua\STD\STD;
use judahnator\Lua\Types\LambdaType;
use judahnator\Lua\Types\Type;

#[Immutable]
final class ApplicationExpression extends Expression
{
    public function __construct(
        public Expression $method,
        public array $args,
    ) {}

    public function __toString(): string
    {
        return sprintf('%s(%s)', $this->method, implode(' ', $this->args));
    }

    public function __debugInfo(): array
    {
        return [
            'method' => $this->method,
            'args' => $this->args,
        ];
    }

    /**
     * Returns the number of tokens involved in this AST node.
     * @return int
     */
    public function count(): int
    {
        return 2 + count($this->method) + self::countList(...$this->args);
    }

    /**
     * Returns the raw scalar type.
     * @param Environment $variables
     * @return Type
     */
    public function getResult(Environment $variables): Type
    {
        // resolve the parent expression
        $method = $this->method->getResult($variables);

        if (!$method instanceof LambdaType) {
            throw new RuntimeException('Cannot use this in a function application.');
        }

        $args = array_map(static fn (Expression $arg): Type => $arg->getResult($variables), $this->args);

        // check to see if the method is named, if so see if it's a built-in function
        if (!is_null($method->name) && !is_null($std = STD::getStdMethod($method->name))) {
            return (new $std($variables))(...$args);
        }

        $env = new Environment($method->scope);
        foreach ($method->args as $index => $arg) {
            /** @var VariableExpression $arg */
            $env->offsetSetLocal($arg->token->getLiteral(), $args[$index]);
        }
        return (new Parser($env, $method->body))->run();
    }
}