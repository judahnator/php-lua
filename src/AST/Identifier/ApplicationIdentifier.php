<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Identifier;

use Judahnator\Lexer\Buffer\TokenBuffer;
use judahnator\Lua\AST\AST;
use judahnator\Lua\AST\Compiler;
use judahnator\Lua\AST\Contracts\InfixIdentifier;
use judahnator\Lua\AST\Expression\ApplicationExpression;
use judahnator\Lua\AST\Expression\Expression;
use judahnator\Lua\AST\SignatureParser;

final class ApplicationIdentifier implements InfixIdentifier
{
    public function matches(Expression $left, TokenBuffer $tokens, Compiler $compiler): AST
    {
        [$args] = SignatureParser::matches(
            '(%E)',
            $tokens
        );

        return new ApplicationExpression($left, $args);
    }
}