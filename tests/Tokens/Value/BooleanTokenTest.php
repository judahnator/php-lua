<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\Tokens\Value;

use JetBrains\PhpStorm\ArrayShape;
use Judahnator\Lexer\Buffer\CharacterBuffer as Characters;;
use Judahnator\Lexer\TokenMismatchException;
use judahnator\Lua\Lexer\Tokens\Token;
use judahnator\Lua\Lexer\Tokens\Value\BooleanToken;
use judahnator\Lua\Types\BooleanType;

/**
 * Class BooleanTokenTest
 * @package judahnator\Lua\Tests\Tokens\Value
 * @covers \judahnator\Lua\Lexer\Tokens\Value\BooleanToken
 */
final class BooleanTokenTest extends ValueTokenTestCase
{

    #[ArrayShape([["callable", TokenMismatchException::class]])] public function provideConstructionExceptionData(): array
    {
        return [
            'false must be "false"' => [
                static fn () => new BooleanToken(new Characters('falze')),
                new TokenMismatchException('Boolean tokens must be exactly "true" or "false".'),
            ],
            'true must be "true"' => [
                static fn () => new BooleanToken(new Characters('troo')),
                new TokenMismatchException('Boolean tokens must be exactly "true" or "false".'),
            ],
        ];
    }

    public function provideTokenData(): array
    {
        return [
            'true is true' => [
                'true', 'BOOL', 4, 'true',
            ],
            'false is false' => [
                'false', 'BOOL', 5, 'false',
            ],
        ];
    }

    public function provideTokenValueData(): array
    {
        return [
            'true is true val' => [
                'true', new BooleanType(true),
            ],
            'false is false val' => [
                'false', new BooleanType(false),
            ]
        ];
    }
}