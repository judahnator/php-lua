<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Expression;

use JetBrains\PhpStorm\Immutable;
use JetBrains\PhpStorm\Pure;
use judahnator\Lua\Environment;
use judahnator\Lua\Types\LambdaType;
use judahnator\Lua\Types\Type;

#[Immutable]
final class ReturnExpression extends Expression
{
    public function __construct(
        private Expression $expression,
    ) {}

    public function __debugInfo(): array
    {
        return [
            'expression' => $this->expression,
        ];
    }

    public function __toString(): string
    {
        return "return {$this->expression}";
    }

    /**
     * Returns the number of tokens involved in this AST node.
     * @return int
     */
    #[Pure] public function count(): int
    {
        return 1 + count($this->expression);
    }

    /**
     * Returns the raw scalar type.
     * @param Environment $variables
     * @return Type
     */
    public function getResult(Environment $variables): Type
    {
        $expression = $this->expression->getResult($variables);
        if ($expression instanceof LambdaType) {
            $expression->scope = $variables;
        }
        return $expression;
    }
}
