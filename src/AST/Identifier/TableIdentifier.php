<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Identifier;

use Judahnator\Lexer\Buffer\TokenBuffer;
use judahnator\Lua\AST\Compiler;
use judahnator\Lua\AST\AST;
use judahnator\Lua\AST\Contracts\PrefixIdentifier;
use judahnator\Lua\AST\Expression\TableExpression;
use judahnator\Lua\AST\SignatureParser;
use judahnator\Lua\Exceptions\AST\MismatchException;
use judahnator\Lua\Types\TableType;

final class TableIdentifier implements PrefixIdentifier
{
    public function matches(TokenBuffer $tokens, Compiler $compiler): AST
    {
        $offset = 0;

        if ($tokens->readOne($offset)->getLiteral() !== '{') {
            throw new MismatchException('Tables must start with an open curly bracket');
        }
        $offset++;

        $table = [];

        while (true) {
            try {
                [$key, $value] = SignatureParser::matches('%i : %e', $tokens->slice($offset));
                $table[$key->getLiteral()] = $value;
                $offset += 2 + count($value);    
            } catch (MismatchException) {
                break;
            }    
        }

        if ($tokens->readOne($offset)->getLiteral() !== '}') {
            throw new MismatchException('Tables must conclude with a closing curly bracket');
        }

        return new TableExpression($table);
    }
}