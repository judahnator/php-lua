<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Identifier;

use Judahnator\Lexer\Buffer\TokenBuffer;
use judahnator\Lua\AST\AST;
use judahnator\Lua\AST\Compiler;
use judahnator\Lua\AST\Contracts\PrefixIdentifier;
use judahnator\Lua\AST\Expression\LiteralExpression;
use judahnator\Lua\Exceptions\AST\MismatchException;
use judahnator\Lua\Lexer\Tokens\Value\Token as ValueToken;

final class LiteralIdentifier implements PrefixIdentifier
{
    public function matches(TokenBuffer $tokens, Compiler $compiler): AST
    {
        if (!($token = $tokens->readOne()) instanceof ValueToken) {
            throw new MismatchException('The provided token does not represent a value.');
        }
        return new LiteralExpression($token);
    }
}