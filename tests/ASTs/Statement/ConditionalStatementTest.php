<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\ASTs\Statement;

use judahnator\Lua\AST\Statement\Conditional;

/**
 * Class ConditionalStatementTest
 * @package judahnator\Lua\Tests\ASTs\Statement
 * @covers \judahnator\Lua\AST\Statement\Conditional
 */
final class ConditionalStatementTest extends StatementTestCase
{
    public function providePrefixGenerationData(): array
    {
        return [
            'if with no else' => [
                'if true then foo = "bar" end', <<<JSON
                    {
                        "type": "Conditional",
                        "token_count": 7,
                        "condition": {
                            "type": "LiteralExpression",
                            "token_count": 1,
                            "token": "true"
                        },
                        "then": [
                            {
                                "type": "AssignmentExpression",
                                "token_count": 3,
                                "variable": {
                                    "type": "VariableExpression",
                                    "token_count": 1,
                                    "token": "foo"
                                },
                                "value": {
                                    "type": "LiteralExpression",
                                    "token_count": 1,
                                    "token": "\"bar\""
                                }
                            }
                        ],
                        "else": []
                    }
                    JSON
            ],
            'if with else' => [
                'if true then foo = "bar" else foo = "baz" end', <<<JSON
                    {
                        "type": "Conditional",
                        "token_count": 11,
                        "condition": {
                            "type": "LiteralExpression",
                            "token_count": 1,
                            "token": "true"
                        },
                        "then": [
                            {
                                "type": "AssignmentExpression",
                                "token_count": 3,
                                "variable": {
                                    "type": "VariableExpression",
                                    "token_count": 1,
                                    "token": "foo"
                                },
                                "value": {
                                    "type": "LiteralExpression",
                                    "token_count": 1,
                                    "token": "\"bar\""
                                }
                            }
                        ],
                        "else": [
                            {
                                "type": "AssignmentExpression",
                                "token_count": 3,
                                "variable": {
                                    "type": "VariableExpression",
                                    "token_count": 1,
                                    "token": "foo"
                                },
                                "value": {
                                    "type": "LiteralExpression",
                                    "token_count": 1,
                                    "token": "\"baz\""
                                }
                            }
                        ]
                    }
                    JSON
            ],
            'if with condition operation' => [
                'if 1 > 2 then foo = "bar" end', <<<JSON
                    {
                        "type": "Conditional",
                        "token_count": 9,
                        "condition": {
                            "type": "OperatorExpression",
                            "token_count": 3,
                            "operator": ">",
                            "lhs": {
                                "type": "LiteralExpression",
                                "token_count": 1,
                                "token": "1"
                            },
                            "rhs": {
                                "type": "LiteralExpression",
                                "token_count": 1,
                                "token": "2"
                            }
                        },
                        "then": [
                            {
                                "type": "AssignmentExpression",
                                "token_count": 3,
                                "variable": {
                                    "type": "VariableExpression",
                                    "token_count": 1,
                                    "token": "foo"
                                },
                                "value": {
                                    "type": "LiteralExpression",
                                    "token_count": 1,
                                    "token": "\"bar\""
                                }
                            }
                        ],
                        "else": []
                    }
                    JSON
            ],
        ];
    }

    public function provideParseData(): array
    {
        return [
            'true statements return first block' => [
                'if true then foo = bar end', <<<JSON
                    [
                        {
                            "type": "AssignmentExpression",
                            "token_count": 3,
                            "variable": {
                                "type": "VariableExpression",
                                "token_count": 1,
                                "token": "foo"
                            },
                            "value": {
                                "type": "VariableExpression",
                                "token_count": 1,
                                "token": "bar"
                            }
                        }
                    ]
                    JSON,
            ],
            'false statements return second block' => [
                'if false then foo = bar else bing = baz end', <<<JSON
                    [
                        {
                            "type": "AssignmentExpression",
                            "token_count": 3,
                            "variable": {
                                "type": "VariableExpression",
                                "token_count": 1,
                                "token": "bing"
                            },
                            "value": {
                                "type": "VariableExpression",
                                "token_count": 1,
                                "token": "baz"
                            }
                        }
                    ]
                    JSON,
            ],
            'empty false statements return simple array' => [
                'if false then foo = "bar" end', <<<JSON
                    []
                    JSON,
            ],
            'condition block can be an operation' => [
                'if 1 < 2 then foo = "bar" end', <<<JSON
                    [
                        {
                            "type": "AssignmentExpression",
                            "token_count": 3,
                            "variable": {
                                "type": "VariableExpression",
                                "token_count": 1,
                                "token": "foo"
                            },
                            "value": {
                                "type": "LiteralExpression",
                                "token_count": 1,
                                "token": "\"bar\""
                            }
                        }
                    ]
                    JSON,
            ]
        ];
    }

    public function provideCountData(): array
    {
        return [
            ['if true then return "foo" end', 6],
            ['if false then return "foo" else return "bar" end', 9],
            ['if foo <= bar then foo = foo + 1 else bar = bar + 1 end', 17]
        ];
    }

    public function provideJsonSerializeData(): array
    {
        return [
            [
                'if true then return true else return false end',
                <<<JSON
                {
                    "type": "Conditional",
                    "token_count": 9,
                    "condition": {
                        "type": "LiteralExpression",
                        "token_count": 1,
                        "token": "true"
                    },
                    "then": [
                        {
                            "type": "ReturnExpression",
                            "token_count": 2,
                            "expression": {
                                "type": "LiteralExpression",
                                "token_count": 1,
                                "token": "true"
                            }
                        }
                    ],
                    "else": [
                        {
                            "type": "ReturnExpression",
                            "token_count": 2,
                            "expression": {
                                "type": "LiteralExpression",
                                "token_count": 1,
                                "token": "false"
                            }
                        }
                    ]
                }
                JSON,
            ],
        ];
    }

    public function provideStringableData(): array
    {
        return [
            'basic condition' => ['if condition then return foo end'],
            'include an else' => ['if foo then return bar else return baz end'],
        ];
    }
}