<?php declare(strict_types=1);

namespace judahnator\Lua\Exceptions\AST;

use RuntimeException as BaseException;

final class RuntimeException extends BaseException
{
}