<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\Types;

use judahnator\Lua\Types\NilType;

/**
 * Class NilTypeTest
 * @package judahnator\Lua\Tests\Types
 * @covers \judahnator\Lua\Types\NilType
 */
final class NilTypeTest extends TypeTestCase
{
    public function provideBooleanData(): array
    {
        return [
            'all nil are false' => [
                new NilType(), false,
            ],
        ];
    }

    public function provideStringableData(): array
    {
        return [
            'all nil are "nil"' => ['nil', new NilType()],
        ];
    }

    public function provideValueData(): array
    {
        return [
            'all nil are null' => [
                new NilType(), null,
            ]
        ];
    }
}