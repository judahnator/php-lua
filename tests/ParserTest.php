<?php declare(strict_types=1);

namespace judahnator\Lua\Tests;

use DateInterval;
use judahnator\Lua\Exceptions\TimeoutException;
use judahnator\Lua\Parser;
use judahnator\Lua\STDIO;
use judahnator\Lua\Types\ListType;
use judahnator\Lua\Types\NilType;
use judahnator\Lua\Types\NumberType;
use PHPUnit\Framework\TestCase;

/**
 * Class ParserTest
 * @package judahnator\Lua\Tests
 * @covers \judahnator\Lua\Parser
 */
final class ParserTest extends TestCase
{
    public function testHelloWorldAssignment(): void
    {
        $parser = self::parse('hello = "Hello World!"');
        $this->assertEquals('Hello World!', $parser->getEnv()->offsetGet('hello')->getValue());
    }

    public function testBasicOperator(): void
    {
        $parser = self::parse(
            <<<SCRIPT
            foo = 10
            bar = foo + 20
            SCRIPT
        );
        $env = $parser->getEnv();
        $this->assertEquals(10, $env->offsetGet('foo')->getValue());
        $this->assertEquals(30, $env->offsetGet('bar')->getValue());
    }

    public function testIf(): void
    {
        // Ensure the 'then' statement runs if condition is true
        $parser = self::parse(
            <<<SCRIPT
            myFoo = "foo"
            if true then myFoo = "bar" end
            SCRIPT
        );
        $this->assertEquals('bar', $parser->getEnv()->offsetGet('myFoo')->getValue());

        // Ensure the 'then' statement does not run if condition is false
        $parser = self::parse(
            <<<SCRIPT
            myFoo = true
            if false then myFoo = false end
            SCRIPT
        );
        $this->assertTrue($parser->getEnv()->offsetGet('myFoo')->getValue());
    }

    public function testElse(): void
    {
        // ensure 'else' runs if condition is false
        $parser = self::parse(
            <<<SCRIPT
            if false then trueVal = 'true' else falseVal = 'false' end
            SCRIPT
        );
        $this->assertFalse($parser->getEnv()->offsetExists('trueVal'));
        $this->assertTrue($parser->getEnv()->offsetExists('falseVal'));

        // ensure 'else' does not run if condition is true
        $parser = self::parse(
            <<<SCRIPT
            if true then trueVal = 'true' else falseVal = 'false' end
            SCRIPT
        );
        $this->assertEquals('true', $parser->getEnv()->offsetGet('trueVal')->getValue());
        $this->assertFalse($parser->getEnv()->offsetExists('falseVal'));
    }

    public function testTimeLimits(): void
    {
        $this->expectExceptionObject(new TimeoutException('Timeout exceeded. Execution halted at: ""nothing""'));
        Parser::setup('while true do "nothing" end', timeLimit: new DateInterval('PT1S'))->run();
    }

    public function testWhile(): void
    {
        $parser = self::parse(
            <<<SCRIPT
            counter = 3
            squareMe = 1
            while counter > 0
            do
                counter = counter - 1
                squareMe = squareMe * 2
            end
            SCRIPT
        );
        $this->assertEquals(0, $parser->getEnv()->offsetGet('counter')->getValue());
        $this->assertEquals(8, $parser->getEnv()->offsetGet('squareMe')->getValue());
    }

    public function testEarlyReturn(): void
    {
        $parser = Parser::setup(
            <<<SCRIPT
            greeting = "Hello World!"
            return greeting
            foo = "bar"
            SCRIPT
        );
        $result = $parser->run();
        $this->assertEquals('Hello World!', $result->getValue());
        $this->assertEquals('Hello World!', $parser->getEnv()->offsetGet('greeting')->getValue());
        $this->assertFalse($parser->getEnv()->offsetExists('foo'));
    }

    public function testBasicFunctions(): void
    {
        $parser = self::parse(
            <<<SCRIPT
            function increment ( input )
                return input + 1
            end
            foo = 1
            bar = increment(foo)
            SCRIPT
        );
        $this->assertFalse($parser->getEnv()->offsetExists('input')); // checks scope bleed
        $this->assertEquals(1, $parser->getEnv()->offsetGet('foo')->getValue());
        $this->assertEquals(2, $parser->getEnv()->offsetGet('bar')->getValue());
    }

    public function testComplexFunctions(): void
    {
        $result = Parser::setup(
            <<<SCRIPT
            function sum (a b)
                return a + b
            end
            return sum(10 20)
            SCRIPT
        )->run();
        $this->assertEquals(30, $result->getValue());
    }

    public function testRecursiveFunctions(): void
    {
        $script = <<<SCRIPT
            function fib(nth)
                if nth < 2 then return nth end
                lhs = fib(nth - 1)
                rhs = fib(nth - 2)
                return lhs + rhs
            end
            return fib(8)
            SCRIPT;
        $this->assertEquals(
            21,
            Parser::setup($script)->run()->getValue()
        );
    }

    public function testOutput(): void
    {
        $IO = new STDIO(STDIO::DRIVER_MEMORY);
        Parser::setup('print("Hello World!")', $IO)->run();
        $this->assertEquals(
            "Hello World!",
            $IO->out
        );
    }

    public function testOutputScoping(): void
    {
        $IO = new STDIO(STDIO::DRIVER_MEMORY);
        $parser = self::parse(<<<SCRIPT
            i = 0
            while i < 15 do
                i = i + 1
                print(i ": ")
                if 0 == i % 3 then print("fizz") end
                if 0 == i % 5 then print("buzz") end
                print("\n")
            end
            SCRIPT,
            $IO
        );
        $this->assertEquals(
            <<<FIZZBUZZ
            1: 
            2: 
            3: fizz
            4: 
            5: buzz
            6: fizz
            7: 
            8: 
            9: fizz
            10: buzz
            11: 
            12: fizz
            13: 
            14: 
            15: fizzbuzz
            
            FIZZBUZZ,
            $parser->getEnv()->STDIO->out
        );
    }

    public function testTables(): void
    {
        // Test writing
        $script = <<<SCRIPT
            fooTable = {}
            fooTable["foo"] = "bar"
            print(fooTable["foo"])
            SCRIPT;
        $parser = self::parse($script, new STDIO(STDIO::DRIVER_MEMORY));
        $this->assertEquals("bar", $parser->getEnv()->STDIO->out);

        // Test overwriting
        $script = <<<SCRIPT
            fooTable = {foo: "bar"}
            fooTable["foo"] = "baz"
            print(fooTable["foo"])
            SCRIPT;
        $parser = self::parse($script, new STDIO(STDIO::DRIVER_MEMORY));
        $this->assertEquals("baz", $parser->getEnv()->STDIO->out);

        // Test default
        $script = <<<SCRIPT
            fooTable = {foo: "asdf"}
            print(fooTable["foo"])
            SCRIPT;
        $parser = self::parse($script, new STDIO(STDIO::DRIVER_MEMORY));
        $this->assertEquals("asdf", $parser->getEnv()->STDIO->out);
    }

    /**
     * todo fix bug where functions cannot be a part of a table constructor
     */
    public function testFirstClassFunctions(): void
    {
        $parser = self::parse(
            <<<SCRIPT
            lambda = function () return 'foo' end
            function named() return 'bar' end
            methods = {}
            methods['add'] = function (left right) return left + right end
            methods['sub'] = function (left right) return left - right end
            methods['lambda'] = lambda
            methods['named'] = named
            print(
                methods['add'](10 20) '\n'
                methods['sub'](15 10) '\n'
                methods['lambda']() '\n'
                methods['named']()
            )
            SCRIPT,
            new STDIO(STDIO::DRIVER_MEMORY)
        );
        $this->assertEquals(
            <<<EXPECTED
            30
            5
            foo
            bar
            EXPECTED,
            $parser->getEnv()->STDIO->out
        );

        $parser = self::parse(
            <<<SCRIPT
            half = function (x) return x / 2 end
            third = function (x) return x / 3 end
            function secondOrder (first second input)
                return first(input) + second(input)
            end
            print(secondOrder(half third 6))
            SCRIPT,
            new STDIO(STDIO::DRIVER_MEMORY)
        );
        $this->assertEquals('5', $parser->getEnv()->STDIO->out);

        $parser = self::parse(
            <<<SCRIPT
            divide = function (x) 
                return function (y)
                    return x / y
                end
            end
            print(divide(8)(4))
            SCRIPT,
            new STDIO(STDIO::DRIVER_MEMORY)
        );
        $this->assertEquals('2', $parser->getEnv()->STDIO->out);
    }

    public function testLists(): void
    {
        $parser = self::parse('mylist = []');
        $this->assertEquals([], $parser->getEnv()->offsetGet('mylist')->getValue());

        $parser = self::parse('myListOfNils = [ nil nil ]');
        $this->assertEquals([new NilType(), new NilType()], $parser->getEnv()->offsetGet('myListOfNils')->getValue());

        $parser = self::parse('myListOfList = [[]]');
        $this->assertEquals((new ListType(new ListType()))->getValue(), $parser->getEnv()->offsetGet('myListOfList')->getValue());

        $parser = self::parse('mixedType = [ 40 [] ]');
        $this->assertEquals([new NumberType(40), new ListType()], $parser->getEnv()->offsetGet('mixedType')->getValue());

        $parser = self::parse('myListOfLists = [ [] [] ]');
        $this->assertEquals([new ListType(), new ListType()], $parser->getEnv()->offsetGet('myListOfLists')->getValue());
    }

    private static function parse(string $input, STDIO $IO = null): Parser
    {
        $parser = Parser::setup($input, $IO);
        $parser->run();
        return $parser;
    }
}