<?php declare(strict_types=1);

namespace judahnator\Lua\STD\Library;

use judahnator\Lua\Environment;
use judahnator\Lua\Exceptions\MethodSignatureException;
use judahnator\Lua\Types\NumberType;
use judahnator\Lua\Types\Type;

final class AbsFunction extends Method
{
    public static string $name = 'abs';

    protected function call(Environment $environment, Type ...$args): Type
    {
        if (count($args) !== 1 || !($input = $args[0]) instanceof NumberType) {
            throw new MethodSignatureException('The "abs" function requires a single number input.');
        }
        return new NumberType(abs($input->getValue()));
    }
}