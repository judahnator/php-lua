<?php

namespace judahnator\Lua\AST;

use Generator;
use JetBrains\PhpStorm\Immutable;
use Judahnator\Lexer\Buffer\TokenBuffer as Tokens;
use Judahnator\Lexer\Contract\TokenInterface;
use judahnator\Lua\AST\Identifier\ApplicationIdentifier;
use judahnator\Lua\AST\Identifier\AssignmentIdentifier;
use judahnator\Lua\AST\Identifier\ConditionalIdentifier;
use judahnator\Lua\AST\Identifier\ForLoopIdentifier;
use judahnator\Lua\AST\Identifier\FunctionIdentifier;
use judahnator\Lua\AST\Identifier\LiteralIdentifier;
use judahnator\Lua\AST\Identifier\OffsetIdentifier;
use judahnator\Lua\AST\Identifier\OperatorIdentifier;
use judahnator\Lua\AST\Identifier\ParenthesisIdentifier;
use judahnator\Lua\AST\Identifier\ReturnIdentifier;
use judahnator\Lua\AST\Identifier\VariableIdentifier;
use judahnator\Lua\AST\Identifier\WhileLoopIdentifier;
use judahnator\Lua\AST\Identifier\ListIdentifier;
use judahnator\Lua\AST\Identifier\TableIdentifier;
use judahnator\Lua\AST\Statement\Statement;
use Judahnator\Lexer\Buffer\CharacterBuffer as Characters;
use judahnator\Lua\Exceptions\AST\MismatchException;
use judahnator\Lua\Exceptions\AST\RuntimeException;
use judahnator\Lua\Lexer;

final class Compiler
{
    #[Immutable(Immutable::PRIVATE_WRITE_SCOPE)]
    public int $depth = 0;

    /**
     * @param Tokens $tokens
     * @return Generator<AST>
     */
    public function compile(Tokens $tokens): Generator
    {
        static $prefixIdentifiers = [
            new FunctionIdentifier(),
            new LiteralIdentifier(),
            new ParenthesisIdentifier(),
            new ReturnIdentifier(),
            new VariableIdentifier(),
            new ConditionalIdentifier(),
            new ForLoopIdentifier(),
            new WhileLoopIdentifier(),
            new ListIdentifier(),
            new TableIdentifier(),
        ];
        static $infixIdentifiers = [
            new ApplicationIdentifier(),
            new AssignmentIdentifier(),
            new OffsetIdentifier(),
            new OperatorIdentifier(),
        ];

        while ($tokens->valid()) {
            $infix = $prefix = null;
            $this->depth++;

            foreach ($prefixIdentifiers as $identifier) {
                try {
                    $prefix = $identifier->matches($tokens, $this);
                } catch (MismatchException) {
                    continue;
                }
                break;
            }

            if (is_null($prefix)) {
                if ($tokens->valid()) {
                    throw new RuntimeException("Syntax error: Unexpected value near \"{$tokens->readOne()->getLiteral()}\".");
                } else {
                    throw new RuntimeException('Syntax error: Unexpected EOF.');
                }
            }

            // special case, infix does not apply to statements
            if ($prefix instanceof Statement) {
                $this->depth--;
                yield $prefix;
                $tokens->seek(count($prefix));
                continue;
            }

            while ($tokens->slice(count($infix ?: $prefix))->valid()) {
                foreach ($infixIdentifiers as $identifier) {
                    try {
                        $infix = $identifier->matches(
                            $infix ?: $prefix,
                            $tokens->slice(count($infix ?: $prefix)),
                            $this
                        );
                    } catch (MismatchException) {
                        continue;
                    }
                    continue 2;
                }
                break;
            }

            $this->depth--;

            if ($infix !== null) {
                yield $infix;
                $tokens->seek(count($infix));
            } else {
                yield $prefix;
                $tokens->seek(count($prefix));
            }
        }
    }

    /**
     * @param string $script
     * @return Generator<AST>
     */
    public static function compileString(string $script): Generator
    {
        return (new self())->compile(
            (new Lexer())->tokenize(new Characters($script))
        );
    }

    /**
     * @param Tokens $tokens
     * @param string ...$terminator
     * @return array<AST>
     */
    final public function readNode(Tokens $tokens, string ...$terminator): array
    {
        $statement = [];
        $ourDepth = $this->depth;
        while (
            ($token = $tokens->readOne()) instanceof TokenInterface &&
            (
                $this->depth !== $ourDepth ||
                !in_array($token->getLiteral(), $terminator)
            )
        ) {
            $tokens->seek(count($statement[] = $this->compile($tokens)->current()));
        }
        return $statement;
    }
}
