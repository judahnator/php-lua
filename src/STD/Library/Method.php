<?php declare(strict_types=1);

namespace judahnator\Lua\STD\Library;

use JetBrains\PhpStorm\Immutable;
use judahnator\Lua\Environment;
use judahnator\Lua\Exceptions\MethodSignatureException;
use judahnator\Lua\Types\Type;

#[Immutable]
abstract class Method
{
    public static string $name;

    final public function __construct(private Environment $scope) {}

    public function __invoke(Type ...$args): Type
    {
        return $this->call($this->scope, ...$args);
    }

    /**
     * Parses the input given with the given environment.
     * Throws an exception if the method signature does not match.
     *
     * @param Environment $environment
     * @param Type ...$args
     * @throws MethodSignatureException
     * @return Type
     */
    abstract protected function call(Environment $environment, Type ...$args): Type;
}