<?php declare(strict_types=1);

namespace judahnator\Lua\Exceptions;

use InvalidArgumentException;

final class MethodSignatureException extends InvalidArgumentException
{
}
