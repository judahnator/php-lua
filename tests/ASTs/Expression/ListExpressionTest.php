<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\ASTs\Expression;

use judahnator\Lua\Tests\ASTs\PrefixTest;
use judahnator\Lua\Types\ListType;
use judahnator\Lua\Types\NilType;

/**
 * @covers \judahnator\Lua\AST\Expression\ListExpression
 * @covers \judahnator\Lua\AST\Identifier\ListIdentifier
 */
final class ListExpressionTest extends ExpressionTestCase
{
    use PrefixTest;

    public function providePrefixGenerationData(): array
    {
        return [
            'empty list' => [
                '[]', <<<JSON
                {
                    "type": "ListExpression",
                    "token_count": 2,
                    "contents": []
                }
                JSON,
            ],
            'populated list' => [
                '[nil 42]', <<<JSON
                {
                    "type": "ListExpression",
                    "token_count": 4,
                    "contents": [
                        {
                            "type": "LiteralExpression",
                            "token_count": 1,
                            "token": "nil"
                        },
                        {
                            "type": "LiteralExpression",
                            "token_count": 1,
                            "token": "42"
                        }
                    ]
                }
                JSON,
            ],
        ];
    }

    public function provideResultData(): array
    {
        return [
            'empty list is a ListType' => [
                '[]', new ListType(),
            ],
            'lists can have contents' => [
                '[nil]', new ListType(new NilType()),
            ],
            'lists can be nested' => [
                '[[]]', new ListType(new ListType()),
            ]
        ];
    }

    public function provideStringableData(): array
    {
        return [
            'empty list' => ['[]'],
            'list with one element' => ['[nil]'],
            'list with several elements' => ['[40 2]'],
        ];
    }

    public function provideCountData(): array
    {
        return [
            ['[]', 2],
            ['[nil]', 3],
            ['["foo" "bar"]', 4],
        ];
    }

    public function provideJsonSerializeData(): array
    {
        return [
            [
                '[]', <<<JSON
                {
                    "type": "ListExpression",
                    "token_count": 2,
                    "contents": []
                }
                JSON,
            ],
            [
                '[ nil ]', <<<JSON
                {
                    "type": "ListExpression",
                    "token_count": 3,
                    "contents": [
                        {
                            "type": "LiteralExpression",
                            "token_count": 1,
                            "token": "nil"
                        }
                    ]
                }
                JSON,
            ],
            [
                '[ 42 [] ]', <<<JSON
                {
                    "type": "ListExpression",
                    "token_count": 5,
                    "contents": [
                        {
                            "type": "LiteralExpression",
                            "token_count": 1,
                            "token": "42"
                        },
                        {
                            "type": "ListExpression",
                            "token_count": 2,
                            "contents": []
                        }
                    ]
                }
                JSON,
            ],
        ];
    }
}