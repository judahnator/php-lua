<?php declare(strict_types=1);

namespace judahnator\Lua\Lexer\Tokens\Value;

use JetBrains\PhpStorm\Pure;
use Judahnator\Lexer\Contract\BufferInterface;
use Judahnator\Lexer\TokenMismatchException;
use judahnator\Lua\Buffers\CharacterBuffer;
use judahnator\Lua\Lexer;
use judahnator\Lua\Lexer\Keywords;
use judahnator\Lua\Types\BooleanType;
use judahnator\Lua\Types\Type;

final class BooleanToken implements Token
{
    private BooleanType $value;

    /**
     * Token constructor.
     * If the token matches the buffer then fills this instance, otherwise throws token exception.
     *
     * @param CharacterBuffer $characters
     * @throws TokenMismatchException
     */
    public function __construct(BufferInterface $characters)
    {
        if ($characters->readLiteral(4) === 'true' && $characters->readOne(4)->isWhitespace()) {
            $this->value = new BooleanType(true);
        } else if ($characters->readLiteral(5) === 'false' && $characters->readOne(5)->isWhitespace()) {
            $this->value = new BooleanType(false);
        } else {
            throw new TokenMismatchException('Boolean tokens must be exactly "true" or "false".');
        }
    }

    public function isWhitespace(): bool
    {
        return false;
    }

    public function getName(): string
    {
        return Keywords::BOOL->name;
    }

    /**
     * Returns the "type" representation of this tokens value.
     * @return Type
     */
    public function getValue(): Type
    {
        return $this->value;
    }

    /**
     * Provides the token length.
     *
     * @return int
     */
    #[Pure] public function count(): int
    {
        return $this->value->toBool() ? 4 : 5;
    }

    /**
     * Returns the literal value of the token.
     *
     * @return string
     */
    #[Pure] public function getLiteral(): string
    {
        return $this->value->toBool() ? 'true' : 'false';
    }
}