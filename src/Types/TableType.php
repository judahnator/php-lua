<?php declare(strict_types=1);

namespace judahnator\Lua\Types;

use InvalidArgumentException;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;
use Judahnator\Lexer\Contract\TokenInterface;
use judahnator\Lua\Lexer\Keywords;
use judahnator\Lua\Lexer\Tokens\IdentityToken;
use judahnator\Lua\Lexer\Tokens\Value\TableToken;

final class TableType implements Type
{
    /**
     * TableType constructor.
     * @param array $value
     */
    public function __construct(
        #[ArrayShape([["string" => Type::class]])]
        private array $value
    )
    {
        foreach ($this->value as $key => $pair) {
            if (!$pair instanceof Type) {
                throw new InvalidArgumentException(sprintf('Invalid table value provided for key "%s".', $key));
            }
        }
    }

    public function __toString(): string
    {
        return sprintf(
            '{%s}',
            implode(
                ' ',
                array_map(
                    static fn (string $key, Type $value): string => "{$key}: {$value}",
                    array_keys($this->value),
                    $this->value,
                ),
            ),
        );
    }

    public function getOffset(string $offset): Type
    {
        return $this->value[$offset] ?? new NilType();
    }

    public function &getValue(): array
    {
        return $this->value;
    }

    public function setOffset(string $offset, Type $newValue): void
    {
        $this->value[$offset] = $newValue;
    }

    public function toBool(): bool
    {
        return true;
    }
}