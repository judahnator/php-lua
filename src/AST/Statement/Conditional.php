<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Statement;

use JetBrains\PhpStorm\Immutable;
use judahnator\Lua\AST\AST;
use judahnator\Lua\AST\Expression\Expression;
use judahnator\Lua\Environment;

#[Immutable]
final class Conditional extends Statement
{
    public function __construct(
        private Expression $condition,

        /** @var AST[] $then */
        private array $then,

        /** @var AST[] $else */
        private array $else,
    ) {}

    public function __debugInfo(): array
    {
        return [
            'condition' => $this->condition,
            'then' => $this->then,
            'else' => $this->else,
        ];
    }

    public function __toString(): string
    {
        $condition = sprintf('if %s then %s', $this->condition, implode(' ', $this->then));
        if (!empty($this->else)) $condition .= " else " . implode(' ', $this->else);
        return $condition . ' end';
    }

    /**
     * Returns the number of tokens involved in this AST node.
     * @return int
     */
    public function count(): int
    {
        return
            (empty($this->else) ? 3 : 4) +
            self::countList(...$this->then, ...$this->else) +
            count($this->condition);
    }

    public function parse(Environment $variables): array
    {
        return $this->condition->getResult($variables)->toBool()
            ? $this->then
            : $this->else;
    }
}
