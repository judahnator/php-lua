<?php declare(strict_types=1);

namespace judahnator\Lua\Operators;

use InvalidArgumentException;
use judahnator\Lua\Types\BooleanType;
use judahnator\Lua\Types\NilType;
use judahnator\Lua\Types\NumberType;
use judahnator\Lua\Types\Type;

final class Operator
{
    private function __construct(
        public readonly string $symbol
    ) {
        if (!in_array($this->symbol, ['+', '-', '*', '/', '%', '^', '==', '~=', '>', '>=', '<', '<='])) {
            throw new InvalidArgumentException('Invalid operator provided.');
        }
    }

    public static function get(string $symbol): self
    {
        return new self($symbol);
    }

    public function lambda(): callable
    {
        return match ($this->symbol) {
            // Arithmetic
            '+' => static fn (NumberType $lhs, NumberType $rhs): NumberType => new NumberType($lhs->getValue() + $rhs->getValue()),
            '-' => static fn (NumberType $lhs, NumberType $rhs): NumberType => new NumberType($lhs->getValue() - $rhs->getValue()),
            '*' => static fn (NumberType $lhs, NumberType $rhs): NumberType => new NumberType($lhs->getValue() * $rhs->getValue()),
            '/' => static fn (NumberType $lhs, NumberType $rhs): NumberType => new NumberType($lhs->getValue() / $rhs->getValue()),
            '%' => static fn (NumberType $lhs, NumberType $rhs): NumberType => new NumberType($lhs->getValue() % $rhs->getValue()),
            '^' => static fn (NumberType $lhs, NumberType $rhs): NumberType => new NumberType($lhs->getValue() ** $rhs->getValue()),

            // Relational
            '==' => static fn (NumberType $lhs, NumberType $rhs): BooleanType => new BooleanType($lhs->getValue() === $rhs->getValue()),
            '~=' => static fn (NumberType $lhs, NumberType $rhs): BooleanType => new BooleanType($lhs->getValue() !== $rhs->getValue()),
            '>' => static fn (NumberType $lhs, NumberType $rhs): BooleanType => new BooleanType($lhs->getValue() > $rhs->getValue()),
            '>=' => static fn (NumberType $lhs, NumberType $rhs): BooleanType => new BooleanType($lhs->getValue() >= $rhs->getValue()),
            '<' => static fn (NumberType $lhs, NumberType $rhs): BooleanType => new BooleanType($lhs->getValue() < $rhs->getValue()),
            '<=' => static fn (NumberType $lhs, NumberType $rhs): BooleanType => new BooleanType($lhs->getValue() <= $rhs->getValue()),

            default => static fn (): NilType => new NilType(),
        };
    }

    public function parse(): Type
    {
        return $this->lambda()(...func_get_args());
    }

    public function precedence(): int
    {
        return match ($this->symbol) {
            '^' => 0,
            '*', '/', '%' => 1,
            '+', '-' => 2,
            '==', '~=', '>', '>=', '<', '<=' => 3,
            default => 99,
        };
    }
}