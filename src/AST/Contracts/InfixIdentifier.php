<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Contracts;

use Judahnator\Lexer\Buffer\TokenBuffer;
use judahnator\Lua\AST\AST;
use judahnator\Lua\AST\Compiler;
use judahnator\Lua\AST\Expression\Expression;

interface InfixIdentifier
{
    public function matches(Expression $left, TokenBuffer $tokens, Compiler $compiler): AST;
}