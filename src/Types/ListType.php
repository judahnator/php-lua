<?php declare(strict_types=1);

namespace judahnator\Lua\Types;

final class ListType implements Type
{
    private array $contents;

    public function __construct(Type ...$contents)
    {
        $this->contents = $contents;
    }

    public function __toString(): string
    {
        return '[' . implode(' ', $this->contents) . ']';
    }

    public function getOffset(NumberType $offset): Type
    {
        return $this->contents[$offset->getValue()] ?: new NilType();
    }

    public function &getValue(): array
    {
        return $this->contents;
    }

    public function toBool(): bool
    {
        return (bool)$this->contents;
    }
}