<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\Types;

use judahnator\Lua\Types\StringType;

/**
 * Class StringTypeTest
 * @package judahnator\Lua\Tests\Types
 * @covers judahnator\Lua\Types\StringType
 */
final class StringTypeTest extends TypeTestCase
{
    public function provideBooleanData(): array
    {
        return [
            'empty strings are true' => [
                new StringType(''), true,
            ],
            'filled strings are true' => [
                new StringType('foo'), true,
            ],
        ];
    }

    public function provideStringableData(): array
    {
        return [
            'empty strings are quotes' => ['""', new StringType('')],
            'full strings are verbatim' => ['"foo"', new StringType('foo')],
        ];
    }

    public function provideValueData(): array
    {
        return [
            'empty strings are empty' => [
                new StringType(''), '',
            ],
            'filled strings are filled' => [
                new StringType('foo bar'), 'foo bar',
            ],
        ];
    }
}