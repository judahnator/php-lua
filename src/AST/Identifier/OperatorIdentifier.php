<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Identifier;

use InvalidArgumentException;
use Judahnator\Lexer\Buffer\TokenBuffer;
use judahnator\Lua\AST\AST;
use judahnator\Lua\AST\Compiler;
use judahnator\Lua\AST\Contracts\InfixIdentifier;
use judahnator\Lua\AST\Expression\Expression;
use judahnator\Lua\AST\Expression\OperatorExpression;
use judahnator\Lua\Exceptions\AST\MismatchException;
use judahnator\Lua\Operators\Operator as BaseOperator;

final class OperatorIdentifier implements InfixIdentifier
{
    public function matches(Expression $left, TokenBuffer $tokens, Compiler $compiler): AST
    {
        try {
            $operator = BaseOperator::get($tokens->readOne()->getLiteral());
        } catch (InvalidArgumentException) {
            throw new MismatchException('No suitable operator found.');
        }

        $right = $compiler->compile($tokens->slice(1))->current();
        if (
            $right instanceof OperatorExpression &&
            $operator->precedence() < BaseOperator::get($right->operator->symbol)->precedence()
        ) {
            $right = $right->lhs; // stops infringement of higher priority operators
        }
        return new OperatorExpression($left, $right, $operator);
    }
}