<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Expression;

use judahnator\Lua\AST\Contracts\Writable;
use judahnator\Lua\Environment;
use judahnator\Lua\Exceptions\AST\RuntimeException;
use judahnator\Lua\Types\LambdaType;
use judahnator\Lua\Types\ListType;
use judahnator\Lua\Types\NilType;
use judahnator\Lua\Types\NumberType;
use judahnator\Lua\Types\StringType;
use judahnator\Lua\Types\TableType;
use judahnator\Lua\Types\Type;

final class OffsetExpression extends Expression implements Writable
{
    public function __construct(
        private Expression $parent,
        private Expression $offset,
    ) {}

    public function __debugInfo(): array
    {
        return [
            'parent' => $this->parent,
            'offset' => $this->offset,
        ];
    }

    public function __toString(): string
    {
        return "{$this->parent}[{$this->offset}]";
    }

    public function count(): int
    {
        return 2 + count($this->parent) + count($this->offset);
    }

    public function getResult(Environment $variables): Type
    {
        $parentValue = $this->parent->getResult($variables);
        $offset = $this->offset->getResult($variables);
        if (
            ($parentValue instanceof TableType && $offset instanceof StringType) ||
            ($parentValue instanceof ListType && $offset instanceof NumberType)
        ) {
            return $parentValue->getOffset($offset->getValue());
        }
        return new NilType();
    }

    public function writeValue(Environment $variables, Type $newValue): void
    {
        if (!$this->parent instanceof Writable) {
            throw new RuntimeException('Cannot use ' . get_class($this->parent) . ' in write context.');
        }

        $parentValue = $this->parent->getResult($variables);
        if (!$parentValue instanceof TableType && !$parentValue instanceof ListType) {
            throw new RuntimeException('Cannot use ' . get_class($parentValue) . ' as table.');
        }

        $offset = $this->offset->getResult($variables);
        if (!$offset instanceof StringType && !$offset instanceof NumberType) {
            throw new RuntimeException('Invalid offset type encountered.');
        }

        if ($newValue instanceof LambdaType) {
            $newValue->scope = $variables;
        }

        $parentValue->setOffset($offset->getValue(), $newValue);
    }
}