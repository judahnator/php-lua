<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Identifier;

use Judahnator\Lexer\Buffer\TokenBuffer;
use judahnator\Lua\AST\AST;
use judahnator\Lua\AST\Compiler;
use judahnator\Lua\AST\Contracts\PrefixIdentifier;
use judahnator\Lua\AST\Expression\ParenthesisExpression;
use judahnator\Lua\AST\SignatureParser;

final class ParenthesisIdentifier implements PrefixIdentifier
{
    public function matches(TokenBuffer $tokens, Compiler $compiler): AST
    {
        [$expression] = SignatureParser::matches(
            '(%e)',
            $tokens,
        );

        return new ParenthesisExpression($expression);
    }
}