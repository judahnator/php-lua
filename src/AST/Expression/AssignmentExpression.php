<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Expression;

use JetBrains\PhpStorm\Immutable;
use JetBrains\PhpStorm\Pure;
use judahnator\Lua\Environment;
use judahnator\Lua\Types\Type;

#[Immutable]
final class AssignmentExpression extends Expression
{
    public function __construct(
        public VariableExpression|OffsetExpression $variable,
        public Expression $value,
    ) {}

    public function __toString(): string
    {
        return "{$this->variable} = {$this->value}";
    }

    public function __debugInfo(): array
    {
        return [
            'variable' => $this->variable,
            'value' => $this->value,
        ];
    }

    #[Pure] public function count(): int
    {
        return 1 + count($this->variable) + count($this->value);
    }

    public function getResult(Environment $variables): Type
    {
        return $this->value->getResult($variables);
    }
}