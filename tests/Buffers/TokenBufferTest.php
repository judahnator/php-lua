<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\Buffers;

use ArrayIterator;
use Generator;
use Judahnator\Lexer\Buffer\CharacterBuffer as Characters;
use Judahnator\Lexer\Buffer\TokenBuffer;
use Judahnator\Lexer\Token\Token;
use judahnator\Lua\Lexer;
use judahnator\Lua\Lexer\Tokens\IdentityToken;
use PHPUnit\Framework\TestCase;

/**
 * Class TokenBufferTest
 * @package judahnator\Lua\Tests
 * @covers \judahnator\Lua\Buffers\CharacterBuffer
 */
final class TokenBufferTest extends TestCase
{
    public function testReading(): void
    {
        $buffer = new TokenBuffer(self::generator());

        $this->assertTrue($buffer->valid());

        $this->assertEquals('foo', self::mapToString($buffer->readOne()));

        $this->assertEquals(
            'foo,bar',
            self::mapToString(...$buffer->read(2)),
        );
        $this->assertEquals(
            'bar,bing',
            self::mapToString(...$buffer->read(2, 1)),
        );

        $this->assertTrue($buffer->valid());

        $buffer->seek(2);

        $this->assertEquals(
            'bing,baz,,',
            self::mapToString(...$buffer->read(4)),
        );
    }

    public function testIterator(): void
    {
        $tokens = new TokenBuffer(self::generator());
        $this->assertEquals('foo', $tokens->getIterator()->current()->getLiteral());
    }

    public function testReadingSlice(): void
    {
        $buffer1 = new TokenBuffer(self::generator());
        $buffer2 = $buffer1->slice(1, 2);
        $buffer3 = $buffer1->slice(1);

        $this->assertEquals('foo,bar', self::mapToString(...$buffer1->read(2)));
        $this->assertEquals('bar,bing,', self::mapToString(...$buffer2->read(3)));
        $this->assertEquals('bar,bing,baz,', self::mapToString(...$buffer3->read(4)));
    }

    private static function generator(): Generator
    {
        $identity = new IdentityToken();
        yield $identity->matches(new Characters('foo'));
        yield $identity->matches(new Characters('bar'));
        yield $identity->matches(new Characters('bing'));
        yield $identity->matches(new Characters('baz'));
    }

    private static function mapToString(?Token ...$tokens): string
    {
        return implode(
            ',',
            array_map(
                static fn (?Token $token): string => $token?->getLiteral() ?: '',
                $tokens,
            ),
        );
    }
}