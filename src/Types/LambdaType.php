<?php declare(strict_types=1);

namespace judahnator\Lua\Types;

use judahnator\Lua\AST\AST;
use judahnator\Lua\Environment;

final class LambdaType implements Type
{
    public Environment $scope;
    public $callback;

    public function __construct(
        public ?string $name,

        /** @var AST[] */
        public array $body,

        /** @var string[] $args */
        public array $args = [],
    ) {}

    public function __toString(): string
    {
        return sprintf(
            'function %s(%s) %s end',
            $this->name ? $this->name . ' ' : '',
            implode(' ', $this->args),
            implode(' ', $this->body),
        );
    }

    public static function fromCallable(string $name, callable $callback): self
    {
        $self = new self($name, [], []);
        $self->callback = $callback;
        return $self;
    }

    public function getValue(): self
    {
        return $this;
    }

    public function toBool(): bool
    {
        return true;
    }
}