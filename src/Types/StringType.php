<?php declare(strict_types=1);

namespace judahnator\Lua\Types;

final class StringType implements Type
{
    public function __construct(private string $value)
    {
    }

    public function __toString(): string
    {
        return "\"{$this->value}\"";
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function toBool(): bool
    {
        return true;
    }
}