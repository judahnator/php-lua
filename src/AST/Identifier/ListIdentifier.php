<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Identifier;

use Judahnator\Lexer\Buffer\TokenBuffer;
use judahnator\Lua\AST\Compiler;
use judahnator\Lua\AST\AST;
use judahnator\Lua\AST\Contracts\PrefixIdentifier;
use judahnator\Lua\AST\Expression\ListExpression;
use judahnator\Lua\AST\SignatureParser;

final class ListIdentifier implements PrefixIdentifier
{
    public function matches(TokenBuffer $tokens, Compiler $compiler): AST
    {
        [$contents] = SignatureParser::matches('[%E]', $tokens);
        return new ListExpression(...$contents);
    }
}