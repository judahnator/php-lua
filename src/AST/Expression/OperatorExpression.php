<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Expression;

use JetBrains\PhpStorm\Immutable;
use JetBrains\PhpStorm\Pure;
use judahnator\Lua\Environment;
use judahnator\Lua\Operators\Operator as BaseOperator;
use judahnator\Lua\Types\Type;

#[Immutable]
final class OperatorExpression extends Expression
{
    public function __construct(
        public readonly Expression $lhs,
        public readonly Expression $rhs,
        public readonly BaseOperator $operator,
    ) {}

    public function __debugInfo(): array
    {
        return [
            'operator' => $this->operator->symbol,
            'lhs' => $this->lhs,
            'rhs' => $this->rhs,
        ];
    }

    public function __toString(): string
    {
        return "{$this->lhs} {$this->operator->symbol} {$this->rhs}";
    }

    /**
     * Returns the number of tokens involved in this AST node.
     * @return int
     */
    #[Pure] public function count(): int
    {
        return 1 + count($this->lhs) + count($this->rhs);
    }

    /**
     * Returns the raw scalar type.
     * @param Environment $variables
     * @return Type
     */
    public function getResult(Environment $variables): Type
    {
        // If the $rhs of this operation is another operator with the same precedence,
        // then "steal" the left side of that as part of a new operation.
        // This effectively turns "1 - 2 + 3" from "1 - (2 + 3)" into "(1 - 2) + 3"
        if (
            $this->rhs instanceof OperatorExpression &&
            $this->operator->precedence() === $this->rhs->operator->precedence()
        ) {
            return (new self(
                new self($this->lhs, $this->rhs->lhs, $this->operator),
                $this->rhs->rhs,
                $this->rhs->operator,
            ))->getResult($variables);
        }

        return $this->operator->parse(
            $this->lhs->getResult($variables),
            $this->rhs->getResult($variables),
        );
    }
}