<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\ASTs\Expression;

use judahnator\Lua\Parser;
use judahnator\Lua\Tests\ASTs\PrefixTest;
use judahnator\Lua\Types\NumberType;
use judahnator\Lua\Types\StringType;

/**
 * Class ApplicationExpressionTest
 * @package judahnator\Lua\Tests\ASTs\Expression
 * @covers \judahnator\Lua\AST\Expression\ApplicationExpression
 */
final class ApplicationExpressionTest extends ExpressionTestCase
{
    use PrefixTest;

    public function providePrefixGenerationData(): array
    {
        return [
            'applications with no input' => [
                'foo()', <<<JSON
                    {
                        "type": "ApplicationExpression",
                        "token_count": 3,
                        "method": {
                            "type": "VariableExpression",
                            "token_count": 1,
                            "token": "foo"
                        },
                        "args": []
                    }
                    JSON,
            ],
            'application with inputs' => [
                'foo(bar baz bing)', <<<JSON
                    {
                        "type": "ApplicationExpression",
                        "token_count": 6,
                        "method": {
                            "type": "VariableExpression",
                            "token_count": 1,
                            "token": "foo"
                        },
                        "args": [
                            {
                                "type": "VariableExpression",
                                "token_count": 1,
                                "token": "bar"
                            },
                            {
                                "type": "VariableExpression",
                                "token_count": 1,
                                "token": "baz"
                            },
                            {
                                "type": "VariableExpression",
                                "token_count": 1,
                                "token": "bing"
                            }
                        ]
                    }
                    JSON,
            ],
            'application with expression' => [
                'foo(1 < 2)', <<<JSON
                    {
                        "type": "ApplicationExpression",
                        "token_count": 6,
                        "method": {
                            "type": "VariableExpression",
                            "token_count": 1,
                            "token": "foo"
                        },
                        "args": [
                            {
                                "type": "OperatorExpression",
                                "token_count": 3,
                                "operator": "<",
                                "lhs": {
                                    "type": "LiteralExpression",
                                    "token_count": 1,
                                    "token": "1"
                                },
                                "rhs": {
                                    "type": "LiteralExpression",
                                    "token_count": 1,
                                    "token": "2"
                                }
                            }
                        ]
                    }
                    JSON
            ]
        ];
    }

    public function provideResultData(): array
    {
        ($parser = Parser::setup(
            <<<SCRIPT
            function identity (input) return input end
            myString = "Hello!"
            SCRIPT
        ))->run();
        $env = $parser->getEnv();
        return [
            'test passing literal' => [
                'identity("asdf")', new StringType("asdf"), $env,
            ],
            'test passing variable' => [
                'identity(myString)', $env->offsetGet('myString'), $env,
            ],
            'test passing expression' => [
                'identity(40 + 2)', new NumberType(42), $env,
            ],
        ];
    }

    public function provideStringableData(): array
    {
        return [
            'application with no args' => ['foo()'],
            'application with one arg' => ['foo(bar)'],
            'application with several args' => ['foo("bar" baz)'],
        ];
    }

    public function provideCountData(): array
    {
        return [
            ['foo()', 3],
            ['foo(bar <= baz)', 6],
            ['foo(bar("baz"))', 7],
        ];
    }

    public function provideJsonSerializeData(): array
    {
        return [
            [
                'foo()',
                <<<JSON
                {
                    "type": "ApplicationExpression",
                    "token_count": 3,
                    "method": {
                        "type": "VariableExpression",
                        "token_count": 1,
                        "token": "foo"
                    },
                    "args": []
                }
                JSON
            ],
            [
                'foo(foo bar < baz)',
                <<<JSON
                {
                    "type": "ApplicationExpression",
                    "token_count": 7,
                    "method": {
                        "type": "VariableExpression",
                        "token_count": 1,
                        "token": "foo"
                    },
                    "args": [
                        {
                            "type": "VariableExpression",
                            "token_count": 1,
                            "token": "foo"
                        },
                        {
                            "type": "OperatorExpression",
                            "token_count": 3,
                            "operator": "<",
                            "lhs": {
                                "type": "VariableExpression",
                                "token_count": 1,
                                "token": "bar"
                            },
                            "rhs": {
                                "type": "VariableExpression",
                                "token_count": 1,
                                "token": "baz"
                            }
                        }
                    ]
                }
                JSON
            ],
        ];
    }
}
