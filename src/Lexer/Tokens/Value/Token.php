<?php declare(strict_types=1);

namespace judahnator\Lua\Lexer\Tokens\Value;

use JetBrains\PhpStorm\Pure;
use Judahnator\Lexer\Contract\ValueTokenInterface;
use Judahnator\Lexer\TokenMismatchException;
use judahnator\Lua\Buffers\CharacterBuffer;
use judahnator\Lua\Types\Type;

interface Token extends ValueTokenInterface
{
    /**
     * Token constructor.
     * If the token matches the buffer then fills this instance, otherwise throws token exception.
     *
     * @param CharacterBuffer $characters
     * @throws TokenMismatchException
     */
    public function __construct(CharacterBuffer $characters);

    /**
     * Returns the "type" representation of this tokens value.
     * @return Type
     */
    #[Pure] public function getValue(): Type;
}