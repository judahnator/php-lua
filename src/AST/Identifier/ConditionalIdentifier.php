<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Identifier;

use Judahnator\Lexer\Buffer\TokenBuffer;
use judahnator\Lua\AST\AST;
use judahnator\Lua\AST\Compiler;
use judahnator\Lua\AST\Contracts\PrefixIdentifier;
use judahnator\Lua\AST\Expression\Expression;
use judahnator\Lua\AST\Statement\Conditional;
use judahnator\Lua\Exceptions\AST\MismatchException;
use judahnator\Lua\Lexer\Keywords;

final class ConditionalIdentifier implements PrefixIdentifier
{
    public function matches(TokenBuffer $tokens, Compiler $compiler): AST
    {
        // if
        $keyword = $tokens->readOne();
        if ($keyword->getName() !== Keywords::KEYWORD_IF->name) {
            throw new MismatchException('Conditionals must begin with the "if" token.');
        }
        unset ($keyword);
        $offset = 1;

        // condition
        $condition = $compiler->compile($tokens->slice($offset))->current();
        if (!$condition instanceof Expression) {
            throw new MismatchException('Conditionals must be based off an expression.');
        }
        $offset += count($condition);

        // then
        $thenKeyword = $tokens->readOne($offset);
        if ($thenKeyword->getName() !== Keywords::KEYWORD_THEN->name) {
            throw new MismatchException('If statements require a "then" keyword.');
        }
        unset($thenKeyword);
        $offset++;

        $then = $compiler->readNode($tokens->slice($offset), 'end', 'else');
        $offset += AST::countList(...$then);

        // If no else then return early
        $elseKeyword = $tokens->readOne($offset);
        if ($elseKeyword->getName() === Keywords::KEYWORD_END->name) {
            return new Conditional($condition, $then, []);
        }
        if (is_null($elseKeyword)) {
            throw new MismatchException('Conditional statement bodies must conclude with either "else" or "end."');
        }
        unset($elseKeyword);
        $offset++;

        $else = $compiler->readNode($tokens->slice($offset), 'end');
        $offset += AST::countList(...$else);

        if ($tokens->readOne($offset)->getName() !== Keywords::KEYWORD_END->name) {
            throw new MismatchException('Conditional statements must end with the "end" keyword.');
        }

        return new Conditional($condition, $then, $else);
    }
}