<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\STD;

use judahnator\Lua\Environment;
use judahnator\Lua\Exceptions\MethodSignatureException;
use judahnator\Lua\STD\Library\Method;
use judahnator\Lua\STDIO;
use judahnator\Lua\Types\Type;
use PHPUnit\Framework\TestCase;

abstract class MethodTestCase extends TestCase
{
    /** @var class-string<Method> */
    protected string $methodClass;

    abstract public function provideConstructionErrorsData(): array;

    abstract public function provideInvocationData(): array;

    /**
     * @dataProvider provideConstructionErrorsData
     * @param MethodSignatureException $exception
     * @param Type ...$inputs
     */
    final public function testConstructionErrors(MethodSignatureException $exception, Type ...$inputs): void
    {
        $this->expectExceptionObject($exception);

        /** @var Method $method */
        $method = new $this->methodClass(new Environment(IO: new STDIO()));
        $method(...$inputs);
    }

    /**
     * @dataProvider provideInvocationData
     * @param mixed $expected
     * @param Type ...$inputs
     */
    final public function testInvocation($expected, Type ...$inputs): void
    {
        /** @var Method $method */
        $method = new $this->methodClass(new Environment(IO: new STDIO(STDIO::DRIVER_MEMORY)));
        $this->assertEquals($expected, $method(...$inputs)->getValue());
    }
}