<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Statement;

use judahnator\Lua\AST\AST;
use judahnator\Lua\Environment;

abstract class Statement extends AST
{
    /**
     * Returns the AST nodes that must be parsed as a part of this statement.
     *
     * @param Environment $variables
     * @return iterable<AST>
     */
    abstract public function parse(Environment $variables): iterable;
}