<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\ASTs\Expression;

use judahnator\Lua\AST\Expression\LiteralExpression;
use judahnator\Lua\Exceptions\AST\MismatchException;
use judahnator\Lua\Tests\ASTs\PrefixTest;
use judahnator\Lua\Types\BooleanType;
use judahnator\Lua\Types\NumberType;
use judahnator\Lua\Types\StringType;

/**
 * Class LiteralExpressionTest
 * @package judahnator\Lua\Tests\ASTs\Expression
 * @covers \judahnator\Lua\AST\Expression\LiteralExpression
 */
final class LiteralExpressionTest extends ExpressionTestCase
{
    use PrefixTest;

    public function providePrefixGenerationData(): array
    {
        return [
            'booleans are literal' => [
                'false', <<<JSON
                {
                    "type": "LiteralExpression",
                    "token_count": 1,
                    "token": "false"
                }
                JSON
            ],
            'numbers are literal' => [
                '12.34', <<<JSON
                {
                    "type": "LiteralExpression",
                    "token_count": 1,
                    "token": "12.34"
                }
                JSON
            ],
            'strings are literal' => [
                '"words are hard"', <<<JSON
                {
                    "type": "LiteralExpression",
                    "token_count": 1,
                    "token": "\"words are hard\""
                }
                JSON
            ],
        ];
    }

    public function provideResultData(): array
    {
        return [
            'boolean literals are boolean' => [
                'true', new BooleanType(true),
            ],
            'number literals are numbers' => [
                '8675309', new NumberType(8675309),
            ],
            'string literals are strings' => [
                '"asdf"', new StringType("asdf"),
            ]
        ];
    }

    public function provideCountData(): array
    {
        return [
            ['"foo"', 1],
            ['42', 1],
        ];
    }

    public function provideJsonSerializeData(): array
    {
        return [
            [
                '"foo"',
                <<<JSON
                {
                    "type": "LiteralExpression",
                    "token_count": 1,
                    "token": "\"foo\""
                }
                JSON,
            ],
            [
                'nil',
                <<<JSON
                {
                    "type": "LiteralExpression",
                    "token_count": 1,
                    "token": "nil"
                }
                JSON,
            ],
            [
                '42',
                <<<JSON
                {
                    "type": "LiteralExpression",
                    "token_count": 1,
                    "token": "42"
                }
                JSON,
            ],
        ];
    }

    public function provideStringableData(): array
    {
        return [
            'string literal' => ['"foo"'],
            'number literal' => ['42.0'],
        ];
    }
}