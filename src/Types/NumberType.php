<?php declare(strict_types=1);

namespace judahnator\Lua\Types;

final class NumberType implements Type
{
    public function __construct(private int|float $value)
    {
    }

    public function __toString(): string
    {
        return (string)$this->value;
    }

    public function getValue(): int|float
    {
        return $this->value;
    }

    public function toBool(): bool
    {
        return true;
    }
}