<?php declare(strict_types=1);

namespace judahnator\Lua\AST;

use Countable;
use JetBrains\PhpStorm\Pure;
use JsonSerializable;
use Judahnator\Lexer\Buffer\TokenBuffer;
use Judahnator\Lexer\Contract\TokenInterface;
use ReflectionClass;
use Stringable;

abstract class AST implements Countable, JsonSerializable, Stringable
{
    abstract public function __debugInfo(): array;

    /**
     * Returns a string representation of this AST node.
     * Does not necessarily represent the input verbatim.
     * @return string
     */
    abstract public function __toString(): string;

    /**
     * Returns the number of tokens involved in this AST node.
     * @return int
     */
    abstract public function count(): int;

    final public static function countList(self ...$asts): int
    {
        return array_reduce(
            $asts,
            static function (int $carry, self $ast): int {
                return $carry + count($ast);
            },
            0,
        );
    }

    /**
     * Returns the JSON representation of this object.
     *
     * @return array
     */
    final public function jsonSerialize(): array
    {
        return [
            'type' => (new ReflectionClass($this))->getShortName(),
            'token_count' => $this->count(),
        ] + $this->__debugInfo();
    }
}