<?php declare(strict_types=1);

namespace judahnator\Lua\Exceptions;

use RuntimeException;

final class TimeoutException extends RuntimeException
{
}