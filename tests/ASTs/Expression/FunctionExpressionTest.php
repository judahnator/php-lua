<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\ASTs\Expression;

use judahnator\Lua\Tests\ASTs\PrefixTest;
use judahnator\Lua\Types\LambdaType;

/**
 * Class FunctionExpressionTest
 * @package judahnator\Lua\Tests\ASTs\Statement
 * @covers \judahnator\Lua\AST\Expression\FunctionExpression
 */
final class FunctionExpressionTest extends ExpressionTestCase
{
    use PrefixTest;

    public function providePrefixGenerationData(): array
    {
        return [
            'basic definition' => [
                'function increment ( input ) return input + 1 end', <<<JSON
                    {
                        "type": "FunctionExpression",
                        "token_count": 10,
                        "name": "increment",
                        "args": [
                            {
                                "type": "VariableExpression",
                                "token_count": 1,
                                "token": "input"
                            }
                        ],
                        "body": [
                            {
                                "type": "ReturnExpression",
                                "token_count": 4,
                                "expression": {
                                    "type": "OperatorExpression",
                                    "token_count": 3,
                                    "operator": "+",
                                    "lhs": {
                                        "type": "VariableExpression",
                                        "token_count": 1,
                                        "token": "input"
                                    },
                                    "rhs": {
                                        "type": "LiteralExpression",
                                        "token_count": 1,
                                        "token": "1"
                                    }
                                }
                            }
                        ]
                    }
                    JSON
            ],
            'complex definition' => [
                'function sum (a b) return a + b end', <<<JSON
                    {
                        "type": "FunctionExpression",
                        "token_count": 11,
                        "name": "sum",
                        "args": [
                            {
                                "type": "VariableExpression",
                                "token_count": 1,
                                "token": "a"
                            },
                            {
                                "type": "VariableExpression",
                                "token_count": 1,
                                "token": "b"
                            }
                        ],
                        "body": [
                            {
                                "type": "ReturnExpression",
                                "token_count": 4,
                                "expression": {
                                    "type": "OperatorExpression",
                                    "token_count": 3,
                                    "operator": "+",
                                    "lhs": {
                                        "type": "VariableExpression",
                                        "token_count": 1,
                                        "token": "a"
                                    },
                                    "rhs": {
                                        "type": "VariableExpression",
                                        "token_count": 1,
                                        "token": "b"
                                    }
                                }
                            }
                        ]
                    }
                    JSON
            ],
            'no argument definition' => [
                'function returnOne () return 1 end', <<<JSON
                    {
                        "type": "FunctionExpression",
                        "token_count": 7,
                        "name": "returnOne",
                        "args": [],
                        "body": [
                            {
                                "type": "ReturnExpression",
                                "token_count": 2,
                                "expression": {
                                    "type": "LiteralExpression",
                                    "token_count": 1,
                                    "token": "1"
                                }
                            }
                        ]
                    }
                    JSON
            ],
            'nameless definition' => [
                'function () return nil end', <<<JSON
                    {
                        "type": "FunctionExpression",
                        "token_count": 6,
                        "name": null,
                        "args": [],
                        "body": [
                            {
                                "type": "ReturnExpression",
                                "token_count": 2,
                                "expression": {
                                    "type": "LiteralExpression",
                                    "token_count": 1,
                                    "token": "nil"
                                }
                            }
                        ]
                    }
                    JSON,
            ]
        ];
    }

    public function provideParseData(): array
    {
        return [
            'basic definition' => [
                'function increment ( input ) return input + 1 end', <<<JSON
                    [
                        {
                            "type": "ReturnExpression",
                            "token_count": 4,
                            "expression": {
                                "type": "OperatorExpression",
                                "token_count": 3,
                                "operator": "+",
                                "lhs": {
                                    "type": "VariableExpression",
                                    "token_count": 1,
                                    "token": "input"
                                },
                                "rhs": {
                                    "type": "LiteralExpression",
                                    "token_count": 1,
                                    "token": "1"
                                }
                            }
                        }
                    ]
                    JSON
            ]
        ];
    }

    public function provideCountData(): array
    {
        return [
            ['function nil() return nil end', 7],
            ['function adds(lhs rhs) return lhs + rhs end', 11],
            ['function () return nil end', 6],
        ];
    }

    public function provideJsonSerializeData(): array
    {
        return [
            [
                'function adds (a b) return a + b end',
                <<<JSON
                {
                    "type": "FunctionExpression",
                    "token_count": 11,
                    "name": "adds",
                    "args": [
                        {
                            "type": "VariableExpression",
                            "token_count": 1,
                            "token": "a"
                        },
                        {
                            "type": "VariableExpression",
                            "token_count": 1,
                            "token": "b"
                        }
                    ],
                    "body": [
                        {
                            "type": "ReturnExpression",
                            "token_count": 4,
                            "expression": {
                                "type": "OperatorExpression",
                                "token_count": 3,
                                "operator": "+",
                                "lhs": {
                                    "type": "VariableExpression",
                                    "token_count": 1,
                                    "token": "a"
                                },
                                "rhs": {
                                    "type": "VariableExpression",
                                    "token_count": 1,
                                    "token": "b"
                                }
                            }
                        }
                    ]
                }
                JSON,
            ],
            [
                'function nullFunc () return nil end',
                <<<JSON
                {
                    "type": "FunctionExpression",
                    "token_count": 7,
                    "name": "nullFunc",
                    "args": [],
                    "body": [
                        {
                            "type": "ReturnExpression",
                            "token_count": 2,
                            "expression": {
                                "type": "LiteralExpression",
                                "token_count": 1,
                                "token": "nil"
                            }
                        }
                    ]
                }
                JSON,
            ],
            [
                'function () return nil end',
                <<<JSON
                {
                    "type": "FunctionExpression",
                    "token_count": 6,
                    "name": null,
                    "args": [],
                    "body": [
                        {
                            "type": "ReturnExpression",
                            "token_count": 2,
                            "expression": {
                                "type": "LiteralExpression",
                                "token_count": 1,
                                "token": "nil"
                            }
                        }
                    ]
                }
                JSON,
            ],
        ];
    }

    public function provideResultData(): array
    {
        return [
            'test basic function' => [
                'function identity (bar) return bar end',
                new LambdaType('identity', iterator_to_array(self::express('return bar')), ['bar']),
            ],
            'test anonymous' => [
                'function nilfunc () return nil end',
                new LambdaType('nilfunc', iterator_to_array(self::express('return nil')), []),
            ],
            'test second order' => [
                'function divide (x) return function (y) return x / y end end',
                new LambdaType('divide', iterator_to_array(self::express('return function (y) return x / y end')), ['x']),
            ],
        ];
    }

    public function provideStringableData(): array
    {
        return [
            'null function' => ['function nullFunc () return nil end'],
            'complex function' => ['function gte (lhs rhs) return lhs <= rhs end'],
            'anonymous function' => ['function () return nil end'],
        ];
    }
}