<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\ASTs\Expression;

use judahnator\Lua\AST\Expression\AssignmentExpression;
use judahnator\Lua\Tests\ASTs\InfixTest;
use judahnator\Lua\Types\NumberType;
use judahnator\Lua\Types\StringType;

/**
 * Class AssignmentTest
 * @package judahnator\Lua\Tests\ASTs\Expression
 * @covers \judahnator\Lua\AST\Expression\AssignmentExpression
 */
final class AssignmentExpressionTest extends ExpressionTestCase
{
    use InfixTest;

    // input, count, tree
    public function provideInfixGenerationData(): array
    {
        return [
            'simple assignment' => [
                'foo = "bar"', <<<JSON
                    {
                        "type": "AssignmentExpression",
                        "token_count": 3,
                        "variable": {
                            "type": "VariableExpression",
                            "token_count": 1,
                            "token": "foo"
                        },
                        "value": {
                            "type": "LiteralExpression",
                            "token_count": 1,
                            "token": "\"bar\""
                        }
                    }
                    JSON
            ],
            'complex assignment' => [
                'answer = 40 + 2', <<<JSON
                    {
                        "type": "AssignmentExpression",
                        "token_count": 5,
                        "variable": {
                            "type": "VariableExpression",
                            "token_count": 1,
                            "token": "answer"
                        },
                        "value": {
                            "type": "OperatorExpression",
                            "token_count": 3,
                            "operator": "+",
                            "lhs": {
                                "type": "LiteralExpression",
                                "token_count": 1,
                                "token": "40"
                            },
                            "rhs": {
                                "type": "LiteralExpression",
                                "token_count": 1,
                                "token": "2"
                            }
                        }
                    }
                    JSON
            ],
        ];
    }

    public function provideResultData(): array
    {
        return [
            'string assignment returns strings' => [
                'greeting = "Hello World!"', new StringType("Hello World!"),
            ],
            'number assignment returns numbers' => [
                'number = 42', new NumberType(42),
            ],
        ];
    }

    public function provideStringableData(): array
    {
        return [
            'assign basic' => ['foo = bar'],
            'assign expression' => ['foo = bar > baz'],
            'assign assignment' => ['foo = bar = baz'],
        ];
    }

    public function provideCountData(): array
    {
        return [
            ['foo = true', 3],
            ['foo = bar', 3],
            ['foo = bar = baz', 5],
            ['foo = bar <= baz', 5],
            ['foo["bar"][baz] = bing', 9],
        ];
    }

    public function provideJsonSerializeData(): array
    {
        return [
            [
                'foo = bar',
                <<<JSON
                {
                    "type": "AssignmentExpression",
                    "token_count": 3,
                    "variable": {
                        "type": "VariableExpression",
                        "token_count": 1,
                        "token": "foo"
                    },
                    "value": {
                        "type": "VariableExpression",
                        "token_count": 1,
                        "token": "bar"
                    }
                }
                JSON,
            ],
            [
                'foo["bar"] = bing["baz"] + 42',
                <<<JSON
                {
                    "type": "AssignmentExpression",
                    "token_count": 11,
                    "variable": {
                        "type": "OffsetExpression",
                        "token_count": 4,
                        "parent": {
                            "type": "VariableExpression",
                            "token_count": 1,
                            "token": "foo"
                        },
                        "offset": {
                            "type": "LiteralExpression",
                            "token_count": 1,
                            "token": "\"bar\""
                        }
                    },
                    "value": {
                        "type": "OperatorExpression",
                        "token_count": 6,
                        "operator": "+",
                        "lhs": {
                            "type": "OffsetExpression",
                            "token_count": 4,
                            "parent": {
                                "type": "VariableExpression",
                                "token_count": 1,
                                "token": "bing"
                            },
                            "offset": {
                                "type": "LiteralExpression",
                                "token_count": 1,
                                "token": "\"baz\""
                            }
                        },
                        "rhs": {
                            "type": "LiteralExpression",
                            "token_count": 1,
                            "token": "42"
                        }
                    }
                }
                JSON,
            ],
        ];
    }
}
