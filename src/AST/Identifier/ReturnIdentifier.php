<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Identifier;

use Judahnator\Lexer\Buffer\TokenBuffer;
use judahnator\Lua\AST\AST;
use judahnator\Lua\AST\Compiler;
use judahnator\Lua\AST\Contracts\PrefixIdentifier;
use judahnator\Lua\AST\Expression\ReturnExpression;
use judahnator\Lua\AST\SignatureLexer;
use judahnator\Lua\AST\SignatureParser;
use judahnator\Lua\Buffers\CharacterBuffer;

final class ReturnIdentifier implements PrefixIdentifier
{
    public function matches(TokenBuffer $tokens, Compiler $compiler): AST
    {
        [$expression] = SignatureParser::matches('return %e', $tokens);

        return new ReturnExpression($expression);
    }
}