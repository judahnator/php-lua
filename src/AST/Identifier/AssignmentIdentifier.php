<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Identifier;

use Judahnator\Lexer\Buffer\TokenBuffer;
use judahnator\Lua\AST\AST;
use judahnator\Lua\AST\Compiler;
use judahnator\Lua\AST\Contracts\InfixIdentifier;
use judahnator\Lua\AST\Expression\AssignmentExpression;
use judahnator\Lua\AST\Expression\Expression;
use judahnator\Lua\AST\Expression\OffsetExpression;
use judahnator\Lua\AST\Expression\VariableExpression;
use judahnator\Lua\AST\SignatureParser;
use judahnator\Lua\Exceptions\AST\MismatchException;

final class AssignmentIdentifier implements InfixIdentifier
{
    public function matches(Expression $left, TokenBuffer $tokens, Compiler $compiler): AST
    {
        if (!$left instanceof VariableExpression && !$left instanceof OffsetExpression) {
            throw new MismatchException();
        }

        [$expression] = SignatureParser::matches('=%e', $tokens,);

        return new AssignmentExpression($left, $expression);
    }
}