<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Expression;

use judahnator\Lua\AST\AST;
use judahnator\Lua\Environment;
use judahnator\Lua\Types\Type;

abstract class Expression extends AST
{
    /**
     * Returns the raw scalar type.
     * @param Environment $variables
     * @return Type
     */
    abstract public function getResult(Environment $variables): Type;
}
