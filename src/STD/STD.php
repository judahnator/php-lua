<?php declare(strict_types=1);

namespace judahnator\Lua\STD;

use judahnator\Lua\STD\Library\AbsFunction;
use judahnator\Lua\STD\Library\MaxFunction;
use judahnator\Lua\STD\Library\Method;
use judahnator\Lua\STD\Library\MinFunction;
use judahnator\Lua\STD\Library\PrintFunction;

final class STD
{
    /** @var class-string<Method>[] */
    private const LIBRARY = [
        AbsFunction::class,
        MaxFunction::class,
        MinFunction::class,
        PrintFunction::class,
    ];

    /**
     * @param string $method
     * @return ?class-string<Method>
     */
    public static function getStdMethod(string $method): ?string
    {
        foreach (self::LIBRARY as $item) {
            if ($item::$name === $method) {
                return $item;
            }
        }
        return null;
    }
}