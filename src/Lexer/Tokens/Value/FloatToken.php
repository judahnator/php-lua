<?php declare(strict_types=1);

namespace judahnator\Lua\Lexer\Tokens\Value;

use JetBrains\PhpStorm\Pure;
use Judahnator\Lexer\Contract\BufferInterface;
use Judahnator\Lexer\TokenMismatchException;
use judahnator\Lua\Lexer;
use judahnator\Lua\Lexer\Keywords;
use judahnator\Lua\Types\NumberType;
use judahnator\Lua\Types\Type;

final class FloatToken implements Token
{
    private string $literal = '';

    /**
     * Token constructor.
     * If the token matches the buffer then fills this instance, otherwise throws token exception.
     *
     * @param BufferInterface $characters
     * @throws TokenMismatchException
     */
    public function __construct(BufferInterface $characters)
    {
        $offset = 0;
        $decimal = false;
        do {
            $token = $characters->readOne($offset);
            $char = $token->getLiteral();
            if (ctype_digit($char) || ($char === '-' && $this->literal === '')) {
                $this->literal .= $char;
            } else if ($char === '.') {
                if ($this->literal === '') {
                    throw new TokenMismatchException('Floats may not start with a decimal.');
                }
                if ($decimal) {
                    throw new TokenMismatchException('Floats may not have more than one decimal place.');
                }
                $this->literal .= $char;
                $decimal = true;
            } else if ($token->isWhitespace()) {
                break;
            } else {
                throw new TokenMismatchException("Integers may not contain the character \"{$char}\".");
            }
            $offset++;
        } while ($characters->valid());
        if (!$decimal) {
            throw new TokenMismatchException('Floats must have one decimal place.');
        }
        if (str_ends_with($this->literal, '.')) {
            throw new TokenMismatchException('Floats must not end with a decimal.');
        }
    }

    /**
     * Provides the token length.
     *
     * @return int
     */
    #[Pure] public function count(): int
    {
        return strlen($this->literal);
    }

    /**
     * Returns the literal value of the token.
     *
     * @return string
     */
    public function getLiteral(): string
    {
        return $this->literal;
    }

    public function getName(): string
    {
        return Keywords::FLOAT->name;
    }

    /**
     * Returns the "type" representation of this tokens value.
     * @return Type
     */
    #[Pure] public function getValue(): Type
    {
        return new NumberType((float)$this->literal);
    }

    public function isWhitespace(): bool
    {
        return false;
    }
}