<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Contracts;

use judahnator\Lua\Environment;
use judahnator\Lua\Types\Type;

interface Writable
{
    public function writeValue(Environment $variables, Type $newValue): void;
}