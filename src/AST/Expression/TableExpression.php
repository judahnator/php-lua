<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Expression;

use judahnator\Lua\Environment;
use judahnator\Lua\Types\TableType;
use judahnator\Lua\Types\Type;

final class TableExpression extends Expression
{
    public function __construct(private array $table)
    {
    }

    public function __debugInfo(): array
    {
        return [
            'table' => $this->table,
        ];
    }

    public function __toString(): string
    {
        $pairs = '';
        foreach ($this->table as $key => $value) {
            $pairs .= "$key: $value ";
        }
        return '{' . rtrim($pairs) . '}';
    }

    public function count(): int
    {
        return 2 + self::countList(...$this->table) + (2 * count($this->table));
    }

    public function getResult(Environment $variables): Type
    {
        return new TableType(array_map(
            static fn (Expression $value): Type => $value->getResult($variables),
            $this->table
        ));
    }
}