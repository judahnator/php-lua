<?php declare(strict_types=1);

namespace judahnator\Lua\AST;

use Judahnator\Lexer\Buffer\TokenBuffer;
use Judahnator\Lexer\Contract\TokenInterface;
use Judahnator\Lexer\TokenMismatchException;
use judahnator\Lua\AST\Expression\Expression;
use judahnator\Lua\AST\Statement\Statement;
use judahnator\Lua\Buffers\CharacterBuffer;
use judahnator\Lua\Exceptions\AST\MismatchException;
use judahnator\Lua\Exceptions\AST\RuntimeException;
use judahnator\Lua\Lexer\Keywords;
use judahnator\Lua\Lexer\Tokens\Keyword;

final class SignatureParser
{
    /**
     * @param string $signature
     * @param TokenBuffer $pattern
     * @return array<TokenInterface|AST|array>
     * @throws MismatchException
     */
    public static function matches(string $signature, TokenBuffer $pattern): array
    {
        $signature = (new SignatureLexer())->tokenize(CharacterBuffer::fromString($signature));

        $nodes = [];
        $keywords = new Keyword();

        $seek = ['signature' => 0, 'pattern' => 0];
        while ($signature->slice($seek['signature'])->valid()) {
            $token = $signature->readOne($seek['signature']);
            switch ($token->getName()) {
                case 'AST':
                    $node = (new Compiler())->compile($pattern->slice($seek['pattern']))->current();
                    if (!$node instanceof AST) {
                        throw new MismatchException();
                    }
                    $seek['signature'] += count($node);
                    $seek['pattern'] += count($node);
                    $nodes[] = $node;
                    unset($node);
                    break;

                case 'AST_LIST':
                    $list = [];
                    try {
                        while (($node = (new Compiler())->compile($pattern->slice($seek['pattern']))->current()) instanceof AST) {
                            $seek['pattern'] += $node->count();
                            $list[] = $node;
                        }
                    } catch (RuntimeException) {}
                    $nodes[] = $list;
                    unset($list);
                    $seek['signature']++;
                    break;

                case 'EXPRESSION':
                    try {
                        $node = (new Compiler())->compile($pattern->slice($seek['pattern']))->current();
                    } catch (RuntimeException) { // The remaining code may not be valid
                        throw new MismatchException();
                    }
                    if (!$node instanceof Expression) {
                        throw new MismatchException();
                    }
                    $nodes[] = $node;
                    $seek['pattern'] += count($node);
                    $seek['signature']++;
                    unset($node);
                    break;

                case 'EXPRESSION_LIST':
                    $list = [];
                    try {
                        while (($node = (new Compiler())->compile($pattern->slice($seek['pattern']))->current()) instanceof Expression) {
                            $seek['pattern'] += $node->count();
                            $list[] = $node;
                        }
                    } catch (RuntimeException) {}
                    $nodes[] = $list;
                    unset($list);
                    $seek['signature']++;
                    break;

                case 'IDENTITY':
                    $nodes[] = $pattern->readOne($seek['pattern']);
                    $seek['signature']++;
                    $seek['pattern']++;
                    break;

                case 'STATEMENT':
                    $node = (new Compiler())->compile($pattern->slice($seek['pattern']))->current();
                    if (!$node instanceof Statement) {
                        throw new MismatchException();
                    }
                    $nodes[] = $node;
                    $seek['signature'] += count($node);
                    $seek['pattern'] += count($node);
                    unset($node);
                    break;

                // Low Level
                case 'PAREN_OPEN':
                    if ($pattern->readOne($seek['pattern'])->getName() !== Keywords::PAREN_OPEN->name) {
                        throw new MismatchException();
                    }
                    $seek['signature']++;
                    $seek['pattern']++;
                    break;
                case 'PAREN_CLOSE':
                    if ($pattern->readOne($seek['pattern'])->getName() !== Keywords::PAREN_CLOSE->name) {
                        throw new MismatchException();
                    }
                    $seek['signature']++;
                    $seek['pattern']++;
                    break;
                case 'ASSIGN':
                    if ($pattern->readOne($seek['pattern'])->getName() !== Keywords::ASSIGN->name) {
                        throw new MismatchException();
                    }
                    $seek['signature']++;
                    $seek['pattern']++;
                    break;
                case 'BRACKET_OPEN':
                    if ($pattern->readOne($seek['pattern'])->getName() !== Keywords::BRACKET_OPEN->name) {
                        throw new MismatchException();
                    }
                    $seek['signature']++;
                    $seek['pattern']++;
                    break;
                case 'BRACKET_CLOSE':
                    if ($pattern->readOne($seek['pattern'])->getName() !== Keywords::BRACKET_CLOSE->name) {
                        throw new MismatchException();
                    }
                    $seek['signature']++;
                    $seek['pattern']++;
                    break;
                case 'COLON':
                    if ($pattern->readOne($seek['pattern'])?->getName() !== Keywords::COLON->name) {
                        throw new MismatchException();
                    }
                    $seek['signature']++;
                    $seek['pattern']++;
                    break;
                case 'COMMA':
                    if ($pattern->readOne($seek['pattern'])->getName() !== Keywords::COMMA->name) {
                        throw new MismatchException();
                    }
                    $seek['signature']++;
                    $seek['pattern']++;
                    break;

                // catch-all
                default:
                    // Check if a keyword
                    try {
                        $literal = $pattern->readOne($seek['pattern'])->getLiteral();
                        $keyword = $keywords->matches(
                            CharacterBuffer::fromString($literal)
                        );
                    } catch (TokenMismatchException) {
                        throw new MismatchException();
                    }
                    // Check if correct keyword
                    if ($keyword->getName() !== $token->getName()) {
                        throw new MismatchException();
                    }
                    $seek['signature']++;
                    $seek['pattern']++;
                    unset($keyword);
                    break;
            }
        }

        return $nodes;
    }
}