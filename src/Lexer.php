<?php declare(strict_types=1);

namespace judahnator\Lua;

use Judahnator\Lexer\Lexer as BaseLexer;
use Judahnator\Lexer\Contract\BufferInterface;
use Judahnator\Lexer\Contract\DictionaryInterface;
use Judahnator\Lexer\Contract\LexerInterface;
use Judahnator\Lexer\Buffer\TokenBuffer;
use Judahnator\Lexer\Dictionary;
use Judahnator\Lexer\Token\ConstantTokenIdentifier;
use Judahnator\Lexer\Token\VariableTokenIdentifier;
use judahnator\Lua\Lexer\Tokens as Token;

final class Lexer implements LexerInterface
{
    private readonly DictionaryInterface $dictionary;

    public function __construct()
    {
        $this->dictionary = new Dictionary(
            new Token\ValueToken(),
            new Token\Keyword(),
            new Token\IdentityToken(),
            new VariableTokenIdentifier([
                'ADD' => '+',
                'SUB' => '-',
                'MUL' => '*',
                'DIV' => '/',
                'MOD' => '%',
                'POW' => '^',
                'IS' => '==',
                'ISNOT' => '~=',
                'GTE' => '>=',
                'LTE' => '<=',
                'GT' => '>',
                'LT' => '<',
            ]),
            new VariableTokenIdentifier([
                'PAREN_OPEN' => '(',
                'PAREN_CLOSE' => ')',
            ]),
            new VariableTokenIdentifier([
                'SQ_BRACKET_OPEN' => '{',
                'SQ_BRACKET_CLOSE' => '}',
            ]),
            new VariableTokenIdentifier([
                'BRACKET_OPEN' => '[',
                'BRACKET_CLOSE' => ']',
            ]),
            new ConstantTokenIdentifier('ASSIGN', '='),
            new ConstantTokenIdentifier('COMMA', ','),
            new ConstantTokenIdentifier('COLON', ':'),
        );
    }

    /**
     * @param string $char
     * @return bool
     */
    public static function isWhitespaceCharacter(string $char): bool
    {
        return ctype_space($char) || $char === ';' || $char === '';
    }

    /**
     * @return TokenBuffer
     */
    public function tokenize(BufferInterface $tokens): TokenBuffer
    {
        return (new BaseLexer($this->dictionary))->tokenize($tokens);
    }
}