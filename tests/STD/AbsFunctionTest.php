<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\STD;

use judahnator\Lua\Exceptions\MethodSignatureException;
use judahnator\Lua\STD\Library\AbsFunction;
use judahnator\Lua\Types\NumberType;
use judahnator\Lua\Types\StringType;

/**
 * @covers \judahnator\Lua\STD\Library\AbsFunction
 * @covers \judahnator\Lua\STD\Library\Method
 */
final class AbsFunctionTest extends MethodTestCase
{
    protected string $methodClass = AbsFunction::class;

    public function provideConstructionErrorsData(): array
    {
        return [
            [new MethodSignatureException('The "abs" function requires a single number input.'), new StringType('foo')],
            [new MethodSignatureException('The "abs" function requires a single number input.'), new NumberType(0), new NumberType(0)],
        ];
    }

    public function provideInvocationData(): array
    {
        return [
            [42, new NumberType(42)],
            [42, new NumberType(-42)],
        ];
    }
}