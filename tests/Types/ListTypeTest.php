<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\Types;

use judahnator\Lua\Types\ListType;
use judahnator\Lua\Types\NilType;
use judahnator\Lua\Types\NumberType;

/**
 * @covers \judahnator\Lua\Types\ListType
 */
final class ListTypeTest extends TypeTestCase
{
    public function provideBooleanData(): array
    {
        return [
            'empty lists are falsy' => [
                new ListType(), false,
            ],
            'lists with contents are truthy' => [
                new ListType(new NilType), true,
            ],
        ];
    }

    public function provideStringableData(): array
    {
        return [
            'empty lists are brackets' => [
                '[]', new ListType(),
            ],
            'lists encapsulate their contents' => [
                '[40 2]', new ListType(new NumberType(40), new NumberType(2)),
            ],
        ];
    }

    public function provideValueData(): array
    {
        return [
            'empty lists are empty' => [
                new ListType(), [],
            ],
            'lists are lists' => [
                new ListType(new NumberType(1)), [new NumberType(1)]
            ],
        ];
    }
}