<?php declare(strict_types=1);

namespace judahnator\Lua\Lexer\Tokens;

use JetBrains\PhpStorm\Pure;
use Judahnator\Lexer\Buffer\CharacterBuffer as Characters;
use Judahnator\Lexer\TokenMismatchException;
use judahnator\Lua\Operators\Operator as BaseOperator;

final class Operator implements Token
{
    public const TOKENS = [
        '+' => 'ADD',
        '-' => 'SUB',
        '*' => 'MUL',
        '/' => 'DIV',
        '%' => 'MOD',
        '^' => 'POW',

        '==' => 'IS',
        '~=' => 'ISNOT',

        '>=' => 'GTE',
        '<=' => 'LTE',
        '>' => 'GT',
        '<' => 'LT',
    ];

    private string $literal;

    /**
     * Token constructor.
     * If the token matches the buffer then fills this instance, otherwise throws token exception.
     *
     * @param Characters $characters
     * @throws TokenMismatchException
     */
    public function __construct(Characters $characters)
    {
        foreach (array_keys(self::TOKENS) as $token) {
            if ($characters->readLiteral(strlen($token)) === $token) {
                $this->literal = $token;
                return;
            }
        }
        throw new TokenMismatchException('The provided characters do not represent an operator.');
    }

    /**
     * Returns the name of this token.
     *
     * @return string
     */
    public function __toString(): string
    {
        return self::TOKENS[$this->literal];
    }

    /**
     * Provides the token length.
     *
     * @return int
     */
    #[Pure] public function count(): int
    {
        return strlen($this->literal);
    }

    /**
     * Returns the literal value of the token.
     *
     * @return string
     */
    public function getLiteral(): string
    {
        return $this->literal;
    }

    /**
     * Returns the "operator" type for this token.
     * @return BaseOperator
     */
    public function getOperator(): BaseOperator
    {
        return BaseOperator::get($this->literal);
    }
}