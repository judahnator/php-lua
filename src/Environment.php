<?php declare(strict_types=1);

namespace judahnator\Lua;

use ArrayAccess;
use JetBrains\PhpStorm\Immutable;
use JetBrains\PhpStorm\Pure;

final class Environment implements ArrayAccess
{
    #[Immutable(Immutable::CONSTRUCTOR_WRITE_SCOPE)] public STDIO $STDIO;

    private array $vars = [];

    public function __construct(private ?Environment $parent = null, STDIO $IO = null)
    {
        if (is_null($IO)) {
            if (is_null($this->parent)) {
                throw new \LogicException('If no STD is provided then you must provide a parent.');
            }
            $this->STDIO = $this->parent->STDIO;
        } else {
            $this->STDIO = $IO;
        }
    }

    /**
     * @param array<string, mixed> $input
     * @return static
     */
    #[Pure] public static function fromArray(array $input, STDIO $IO): self
    {
        $self = new self(IO: $IO);
        $self->vars = $input;
        return $self;
    }

    /**
     * @param string $offset
     * @return bool
     */
    public function offsetExists($offset): bool
    {
        return array_key_exists($offset, $this->vars) || $this->parent?->offsetExists($offset);
    }

    /**
     * @param mixed $offset
     * @return mixed|null
     */
    public function offsetGet($offset): mixed
    {
        return $this->vars[$offset] ?? $this->parent?->offsetGet($offset);
    }

    /**
     * @param string $offset
     * @param mixed $value
     */
    public function offsetSet($offset, $value): void
    {
        if (array_key_exists($offset, $this->vars) || !$this->parent?->offsetExists($offset)) {
            $this->vars[$offset] = $value;
            return;
        }
        if (!is_null($this->parent)) {
            $this->parent->offsetSet($offset, $value);
        }
    }

    public function offsetSetLocal($offset, $value): void
    {
        $this->vars[$offset] = $value;
    }

    /**
     * @param string $offset
     * @return void
     */
    public function offsetUnset($offset): void
    {
        if ($this->parent?->offsetExists($offset)) {
            $this->parent->offsetUnset($offset);
        }
        if (array_key_exists($offset, $this->vars)) {
            unset($this->vars[$offset]);
        }
    }
}
