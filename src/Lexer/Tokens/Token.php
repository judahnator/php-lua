<?php declare(strict_types=1);

namespace judahnator\Lua\Lexer\Tokens;

use Countable;
use JetBrains\PhpStorm\Pure;
use Judahnator\Lexer\Buffer\CharacterBuffer as Characters;
use Judahnator\Lexer\TokenMismatchException;
use Stringable;

interface Token extends Countable, Stringable
{
    /**
     * Token constructor.
     * If the token matches the buffer then fills this instance, otherwise throws token exception.
     *
     * @throws TokenMismatchException
     * @param Characters $characters
     */
    public function __construct(Characters $characters);

    /**
     * Returns the name of this token.
     *
     * @return string
     */
    public function __toString(): string;

    /**
     * Provides the token length.
     *
     * @return int
     */
    #[Pure]
    public function count(): int;

    /**
     * Returns the literal value of the token.
     *
     * @return string
     */
    public function getLiteral(): string;
}