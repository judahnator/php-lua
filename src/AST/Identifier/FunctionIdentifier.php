<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Identifier;

use Judahnator\Lexer\Buffer\TokenBuffer;
use judahnator\Lua\AST\AST;
use judahnator\Lua\AST\Compiler;
use judahnator\Lua\AST\Contracts\PrefixIdentifier;
use judahnator\Lua\AST\Expression\FunctionExpression;
use judahnator\Lua\AST\SignatureParser;
use judahnator\Lua\Exceptions\AST\MismatchException;
use judahnator\Lua\Types\LambdaType;

final class FunctionIdentifier implements PrefixIdentifier
{
    public function matches(TokenBuffer $tokens, Compiler $compiler): AST
    {
        // Cannot simply `clone $tokens`, it messes with the inner generators.
        // Instead, take a "whole slice" wih `$tokens->slice(0)`.

        // Try to create a named function, then try to create an anonymous function
        try {
            [$name, $args, $body] = SignatureParser::matches(
                'function %i ( %E ) %A end',
                $tokens->slice(0)
            );
            $lambda = new LambdaType($name->getLiteral(), $body, $args);
        } catch (MismatchException) {
            [$args, $body] = SignatureParser::matches(
                'function ( %E ) %A end',
                $tokens->slice(0)
            );
            $lambda = new LambdaType(null, $body, $args);
        }

        return new FunctionExpression($lambda);
    }
}