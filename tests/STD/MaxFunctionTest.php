<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\STD;

use judahnator\Lua\Exceptions\MethodSignatureException;
use judahnator\Lua\STD\Library\MaxFunction;
use judahnator\Lua\Types\NumberType;
use judahnator\Lua\Types\StringType;

/**
 * @covers \judahnator\Lua\STD\Library\MaxFunction
 * @covers \judahnator\Lua\STD\Library\Method
 */
final class MaxFunctionTest extends MethodTestCase
{
    protected string $methodClass = MaxFunction::class;

    public function provideConstructionErrorsData(): array
    {
        $exception = new MethodSignatureException('The "abs" function requires exactly two parameters of type number.');
        return [
            [$exception, new StringType('foo')],
            [$exception, new NumberType(10)],
            [$exception, new NumberType(10), new NumberType(20), new NumberType(30)],
        ];
    }

    public function provideInvocationData(): array
    {
        return [
            [10, new NumberType(5), new NumberType(10)],
            [0, new NumberType(0), new NumberType(-3)],
        ];
    }
}