<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\ASTs\Expression;

use judahnator\Lua\Tests\ASTs\PrefixTest;
use judahnator\Lua\Types\NilType;
use judahnator\Lua\Types\StringType;
use judahnator\Lua\Types\TableType;

/**
 * @covers judahnator\Lua\AST\Expression\TableExpression
 */
final class TableExpressionTest extends ExpressionTestCase
{
    use PrefixTest;

    public function providePrefixGenerationData(): array
    {
        return [
            'empty table' => [
                '{}', <<<JSON
                {
                    "type": "TableExpression",
                    "token_count": 2,
                    "table": []
                }
                JSON,
            ],
            'populated table' => [
                '{ foo: "bar" bing: 42 }', <<<JSON
                {
                    "type": "TableExpression",
                    "token_count": 8,
                    "table": {
                        "foo": {
                            "type": "LiteralExpression",
                            "token_count": 1,
                            "token": "\"bar\""
                        },
                        "bing": {
                            "type": "LiteralExpression",
                            "token_count": 1,
                            "token": "42"
                        }
                    }
                }
                JSON,
            ],
        ];
    }

    public function provideResultData(): array
    {
        return [
            'empty table is a TableType' => [
                '{}', new TableType([]),
            ],
            'tables can have contents' => [
                '{foo: nil}', new TableType(['foo' => new NilType()]),
            ],
            'tables can be nested' => [
                '{foo: {bar: "baz"}}', new TableType(['foo' => new TableType(['bar' => new StringType('baz')])]),
            ],
        ];
    }

    public function provideStringableData(): array
    {
        return [
            'empty table is empty' => ['{}'],
            'table with element' => ['{foo: nil}'],
            'table with elements' => ['{foo: 40 bar: 2}']
        ];
    }

    public function provideCountData(): array
    {
        return [
            ['{}', 2],
            ['{foo: nil}', 5],
            ['{foo: nil bing: nil}', 8]
        ];
    }

    public function provideJsonSerializeData(): array
    {
        return [
            [
                '{}', <<<JSON
                {
                    "type": "TableExpression",
                    "token_count": 2,
                    "table": []
                }
                JSON
            ],
            [
                '{foo: 42}', <<<JSON
                {
                    "type": "TableExpression",
                    "token_count": 5,
                    "table": {
                        "foo": {
                            "type": "LiteralExpression",
                            "token_count": 1,
                            "token": "42"
                        }
                    }
                }
                JSON
            ]
        ];
    }
}