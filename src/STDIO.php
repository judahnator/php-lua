<?php declare(strict_types=1);

namespace judahnator\Lua;

use InvalidArgumentException;
use JetBrains\PhpStorm\Immutable;

final class STDIO
{
    public const DRIVER_MEMORY = 0;
    public const DRIVER_STDIO = 1;

    #[Immutable(Immutable::PRIVATE_WRITE_SCOPE)] public string $err = '';

    #[Immutable(Immutable::PRIVATE_WRITE_SCOPE)] public string $out = '';

    #[Immutable(Immutable::CONSTRUCTOR_WRITE_SCOPE)] private int $driver;

    public function __construct(int $driver = self::DRIVER_STDIO)
    {
        if ($driver !== self::DRIVER_MEMORY && $driver !== self::DRIVER_STDIO) {
            throw new InvalidArgumentException('Invalid driver argument provided.');
        }
        $this->driver = $driver;
    }

    public function err(string $output): void
    {
        switch ($this->driver) {
            case self::DRIVER_MEMORY:
                $this->err .= $output;
                break;
            case self::DRIVER_STDIO:
                fwrite(STDERR, $output);
                break;
        }
    }

    public function out(string $output): void
    {
        switch ($this->driver) {
            case self::DRIVER_MEMORY:
                $this->out .= $output;
                break;
            case self::DRIVER_STDIO:
                fwrite(STDOUT, $output);
                break;
        }
    }
}
