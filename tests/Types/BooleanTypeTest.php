<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\Types;

use judahnator\Lua\Types\BooleanType;

/**
 * Class BooleanTypeTest
 * @package judahnator\Lua\Tests\Types
 * @covers \judahnator\Lua\Types\BooleanType
 */
final class BooleanTypeTest extends TypeTestCase
{
    public function provideBooleanData(): array
    {
        return [
            'values can be true' => [
                new BooleanType(true), true,
            ],
            'values can be false' => [
                new BooleanType(false), false,
            ],
        ];
    }

    public function provideStringableData(): array
    {
        return [
            'true is true' => ["true", new BooleanType(true)],
            'false is false' => ["false", new BooleanType(false)],
        ];
    }

    public function provideValueData(): array
    {
        return [
            'true values are true' => [
                new BooleanType(true), true,
            ],
            'false values are false' => [
                new BooleanType(false), false,
            ],
        ];
    }
}