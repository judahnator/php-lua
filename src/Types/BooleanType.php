<?php declare(strict_types=1);

namespace judahnator\Lua\Types;

final class BooleanType implements Type
{
    public function __construct(private bool $value)
    {
    }

    public function __toString(): string
    {
        return $this->value ? "true" : "false";
    }

    public function getValue(): bool
    {
        return $this->value;
    }

    public function toBool(): bool
    {
        return $this->value;
    }
}