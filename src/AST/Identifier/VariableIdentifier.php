<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Identifier;

use Judahnator\Lexer\Buffer\TokenBuffer;
use judahnator\Lua\AST\AST;
use judahnator\Lua\AST\Compiler;
use judahnator\Lua\AST\Contracts\PrefixIdentifier;
use judahnator\Lua\AST\Expression\VariableExpression;
use judahnator\Lua\Exceptions\AST\MismatchException;
use judahnator\Lua\Lexer\Keywords;
use judahnator\Lua\Lexer\Tokens\Keyword;

final class VariableIdentifier implements PrefixIdentifier
{
    public function matches(TokenBuffer $tokens, Compiler $compiler): AST
    {
        $token = $tokens->readOne();
        if ($token->getName() !== Keywords::IDENT->name || in_array($token->getLiteral(), Keyword::KEYWORDS)) {
            throw new MismatchException('The provided token does not represent a variable.');
        }
        return new VariableExpression($token);
    }
}