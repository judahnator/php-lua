<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\ASTs\Expression;

use judahnator\Lua\AST\Expression\OperatorExpression;
use judahnator\Lua\Exceptions\AST\MismatchException;
use judahnator\Lua\Parser;
use judahnator\Lua\Tests\ASTs\InfixTest;
use judahnator\Lua\Types\BooleanType;
use judahnator\Lua\Types\NumberType;

/**
 * Class OperatorExpressionTest
 * @package judahnator\Lua\Tests\ASTs\Expression
 * @covers \judahnator\Lua\AST\Expression\OperatorExpression
 */
final class OperatorExpressionTest extends ExpressionTestCase
{
    use InfixTest;

    public function providePrefixGenerationData(): array
    {
        return [
            'basic addition' => [
                '2 + 3', <<<JSON
                    {
                        "type": "OperatorExpression",
                        "token_count": 3,
                        "operator": "+",
                        "lhs": {
                            "type": "LiteralExpression",
                            "token_count": 1,
                            "token": "2"
                        },
                        "rhs": {
                            "type": "LiteralExpression",
                            "token_count": 1,
                            "token": "3"
                        }
                    }
                    JSON,
            ],
            'complex operations' => [
                '98.76 - 54.32 + 1', <<<JSON
                    {
                        "type": "OperatorExpression",
                        "token_count": 5,
                        "operator": "-",
                        "lhs": {
                            "type": "LiteralExpression",
                            "token_count": 1,
                            "token": "98.76"
                        },
                        "rhs": {
                            "type": "OperatorExpression",
                            "token_count": 3,
                            "operator": "+",
                            "lhs": {
                                "type": "LiteralExpression",
                                "token_count": 1,
                                "token": "54.32"
                            },
                            "rhs": {
                                "type": "LiteralExpression",
                                "token_count": 1,
                                "token": "1"
                            }
                        }
                    }
                    JSON,
            ],
        ];
    }

    public function provideResultData(): array
    {
        return [
            'addition' => [
                '1 + 2', new NumberType(3),
            ],
            'subtraction' => [
                '4 - 2', new NumberType(2),
            ],
            'multiplication' => [
                '40 * 2', new NumberType(80),
            ],
            'division' => [
                '40 / 2', new NumberType(20),
            ],
            'modulus' => [
                '11 % 3', new NumberType(2),
            ],
            'power' => [
                '2 ^ 10', new NumberType(1024),
            ],

            'equality' => [
                '42 == 42', new BooleanType(true),
            ],
            'not equality' => [
                '42 ~= 42', new BooleanType(false),
            ],
        ];
    }

    public function provideCountData(): array
    {
        return [
            ['1 + 2', 3],
            ['1 > 2 + 3', 5],
        ];
    }

    public function provideJsonSerializeData(): array
    {
        return [
            [
                '10 + 20',
                <<<JSON
                {
                    "type": "OperatorExpression",
                    "token_count": 3,
                    "operator": "+",
                    "lhs": {
                        "type": "LiteralExpression",
                        "token_count": 1,
                        "token": "10"
                    },
                    "rhs": {
                        "type": "LiteralExpression",
                        "token_count": 1,
                        "token": "20"
                    }
                }
                JSON,
            ],
            [
                'foo * bar <= baz',
                <<<JSON
                {
                    "type": "OperatorExpression",
                    "token_count": 5,
                    "operator": "<=",
                    "lhs": {
                        "type": "OperatorExpression",
                        "token_count": 3,
                        "operator": "*",
                        "lhs": {
                            "type": "VariableExpression",
                            "token_count": 1,
                            "token": "foo"
                        },
                        "rhs": {
                            "type": "VariableExpression",
                            "token_count": 1,
                            "token": "bar"
                        }
                    },
                    "rhs": {
                        "type": "VariableExpression",
                        "token_count": 1,
                        "token": "baz"
                    }
                }
                JSON,
            ],
        ];
    }

    public function provideInfixGenerationData(): array
    {
        return [
            [
                'foo < bar',
                <<<JSON
                {
                    "type": "OperatorExpression",
                    "token_count": 3,
                    "operator": "<",
                    "lhs": {
                        "type": "VariableExpression",
                        "token_count": 1,
                        "token": "foo"
                    },
                    "rhs": {
                        "type": "VariableExpression",
                        "token_count": 1,
                        "token": "bar"
                    }
                }
                JSON,
            ],
            [
                'foo * bar / baz',
                <<<JSON
                {
                    "type": "OperatorExpression",
                    "token_count": 5,
                    "operator": "*",
                    "lhs": {
                        "type": "VariableExpression",
                        "token_count": 1,
                        "token": "foo"
                    },
                    "rhs": {
                        "type": "OperatorExpression",
                        "token_count": 3,
                        "operator": "\/",
                        "lhs": {
                            "type": "VariableExpression",
                            "token_count": 1,
                            "token": "bar"
                        },
                        "rhs": {
                            "type": "VariableExpression",
                            "token_count": 1,
                            "token": "baz"
                        }
                    }
                }
                JSON,
            ],
        ];
    }

    public function provideStringableData(): array
    {
        return [
            'literal operators' => ['10 < 20'],
            'variable operator' => ['foo > bar'],
        ];
    }

    public function testOperatorPrecedence(): void
    {
        foreach (
            [
                '3 + 4 / 2 - 4' => 1,
                '4 / 2 ^ 2 - 4' => -3,
                '1 + 2 - 3 * 3 + 4 ^ 2' => 10,
                '1 - 2 * 3 * 4' => -23,
                '7 - 1 * 0 + 3 / 3' => 8
            ] as $formula => $expected
        ) {
            $this->assertEquals($expected, Parser::setup("return {$formula}")->run()->getValue());
        }
    }
}
