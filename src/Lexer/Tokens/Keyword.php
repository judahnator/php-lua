<?php declare(strict_types=1);

namespace judahnator\Lua\Lexer\Tokens;

use Judahnator\Lexer\Contract\BufferInterface;
use Judahnator\Lexer\Contract\TokenIdentifierInterface;
use Judahnator\Lexer\Contract\TokenInterface;
use Judahnator\Lexer\Token\VariableTokenIdentifier;
use Judahnator\Lexer\TokenMismatchException;
use judahnator\Lua\Lexer;
use judahnator\Lua\Lexer\Keywords;

final class Keyword implements TokenIdentifierInterface
{
    // sorting by length, elseif > else
    public const KEYWORDS = [
        'function' => Keywords::KEYWORD_FUNCTION,
        'elseif' => Keywords::KEYWORD_ELSEIF,
        'repeat' => Keywords::KEYWORD_REPEAT,
        'return' => Keywords::KEYWORD_RETURN,
        'break' => Keywords::KEYWORD_BREAK,
        'local' => Keywords::KEYWORD_LOCAL,
        'until' => Keywords::KEYWORD_UNTIL,
        'while' => Keywords::KEYWORD_WHILE,
        'else' => Keywords::KEYWORD_ELSE,
        'then' => Keywords::KEYWORD_THEN,
        'and' => Keywords::KEYWORD_AND,
        'end' => Keywords::KEYWORD_END,
        'for' => Keywords::KEYWORD_FOR,
        'not' => Keywords::KEYWORD_NOT,
        'do' => Keywords::KEYWORD_DO,
        'if' => Keywords::KEYWORD_IF,
        'in' => Keywords::KEYWORD_IN,
        'or' => Keywords::KEYWORD_OR,
    ];

    private readonly TokenIdentifierInterface $identifier;

    public function __construct()
    {
        $this->identifier = new VariableTokenIdentifier(
            array_flip(
                array_map(
                    static fn (Keywords $keyword): string => $keyword->name,
                    self::KEYWORDS,
                )
            )
        );
    }

    public function __toString(): string
    {
        return '[' . implode('|', array_keys(self::KEYWORDS)) . ']';
    }

    public function matches(BufferInterface $characters): TokenInterface
    {
        try {
            $match = $this->identifier->matches($characters);
            if (!$characters->readOne(strlen($match->getLiteral()))->isWhitespace()) {
                throw new TokenMismatchException('Keywords must be concluded with whitespace.');
            }
        } catch (TokenMismatchException $e) {
            throw new TokenMismatchException('No matching keyword found.', previous: $e);
        }
        return $match;
    }
}