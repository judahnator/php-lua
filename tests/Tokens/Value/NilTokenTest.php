<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\Tokens\Value;

use Judahnator\Lexer\Buffer\CharacterBuffer as Characters;;
use Judahnator\Lexer\TokenMismatchException;
use judahnator\Lua\Lexer\Tokens\Value\NilToken;
use judahnator\Lua\Types\NilType;

/**
 * @covers \judahnator\Lua\Lexer\Tokens\Value\NilToken
 */
final class NilTokenTest extends ValueTokenTestCase
{
    public function provideConstructionExceptionData(): array
    {
        $exception = new TokenMismatchException('Nil tokens must be exactly nil');
        return [
            'nil must not have trailing characters' => [
                static fn () => new NilToken(new Characters('nilb')),
                $exception,
            ],
            'nil must be exactly nil' => [
                static fn () => new NilToken(new Characters('nul')),
                $exception,
            ]
        ];
    }

    public function provideTokenData(): array
    {
        return [
            'nil is exactly nil' => [
                'nil', 'NIL', 3, 'nil',
            ],
            'nil terminates on whitespace' => [
                'nil 123', 'NIL', 3, 'nil',
            ],
        ];
    }

    public function provideTokenValueData(): array
    {
        return [
            'nil is nil' => [
                'nil', new NilType(),
            ]
        ];
    }
}