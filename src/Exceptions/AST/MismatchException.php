<?php declare(strict_types=1);

namespace judahnator\Lua\Exceptions\AST;

use InvalidArgumentException;

final class MismatchException extends InvalidArgumentException
{
}