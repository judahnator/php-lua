<?php declare(strict_types=1);

namespace judahnator\Lua\Tests;

use judahnator\Lua\Environment;
use judahnator\Lua\STDIO;
use PHPUnit\Framework\TestCase;

/**
 * Class EnvironmentTest
 * @package judahnator\Lua\Tests
 * @covers \judahnator\Lua\Environment
 * @uses \judahnator\Lua\STDIO
 */
final class EnvironmentTest extends TestCase
{
    public function testBasic(): void
    {
        $env = new Environment(IO: new STDIO());
        $env['foo'] = 'bar';
        $env['bing'] = 'baz';
        unset($env['bing']);

        $this->assertTrue(isset($env['foo']));
        $this->assertFalse(isset($env['bing']));

        $this->assertEquals('bar', $env['foo']);
        $this->assertNull($env['bing']);
    }

    public function testCreatingFromArray(): void
    {
        $env = Environment::fromArray(['foo' => 'bar'], new STDIO());
        $this->assertTrue(isset($env['foo']));
        $this->assertEquals('bar', $env['foo']);
    }

    public function testInheritance(): void
    {
        $l0 = new Environment(IO: new STDIO());
        $l1 = new Environment($l0);
        $l2 = new Environment($l1);

        // Ensure the IO is the same object across envs
        $this->assertSame($l0->STDIO, $l1->STDIO);
        $this->assertSame($l0->STDIO, $l2->STDIO);

        $l0['foo'] = 'foo';
        $l1['bar'] = 'bar';
        $l2['bing'] = 'bing';

        // test accessing
        $this->assertEquals('foo', $l2['foo']);
        $this->assertEquals('bar', $l2['bar']);
        $this->assertEquals('bing', $l2['bing']);

        // test updating
        $l2['foo'] = 'foobar';
        $this->assertEquals('foobar', $l2['foo']);
        $this->assertEquals('foobar', $l0['foo']);

        unset($l0['foo']);
        unset($l2['bing']);

        // test unsetting
        $this->assertFalse(isset($l2['foo']));
        $this->assertFalse(isset($l2['bing']));
    }

    public function testUnsettingParentElements(): void
    {
        $l0 = new Environment(IO: new STDIO());
        $l1 = new Environment($l0);

        $l0['foo'] = 'bar';
        $this->assertTrue(isset($l0['foo']));
        $this->assertTrue(isset($l1['foo']));

        unset($l1['foo']);

        $this->assertFalse(isset($l0['foo']));
        $this->assertFalse(isset($l1['foo']));
    }

    public function testConstructionException(): void
    {
        $this->expectExceptionObject(new \LogicException('If no STD is provided then you must provide a parent.'));
        new Environment();
    }

    public function testLocalVariableInteraction(): void
    {
        $io = new STDIO(STDIO::DRIVER_MEMORY);
        $parent = Environment::fromArray(['foo' => 'bar'], $io);
        $child = new Environment($parent, $io);
        $child->offsetSetLocal('foo', 'baz');
        $this->assertEquals('bar', $parent->offsetGet('foo'));
        $this->assertEquals('baz', $child->offsetGet('foo'));
    }
}