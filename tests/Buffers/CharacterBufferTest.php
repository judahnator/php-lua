<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\Buffers;

use Generator;
use JetBrains\PhpStorm\Pure;
use Judahnator\Lexer\Buffer\CharacterBuffer as Characters;;
use PHPUnit\Framework\TestCase;

/**
 * Class CharacterBufferTest
 * @package judahnator\Lua\Tests
 * @covers \judahnator\Lua\Buffers\CharacterBuffer
 */
final class CharacterBufferTest extends TestCase
{
    public function testFromStringFunction(): void
    {
        $buffer = new Characters('asdf');
        $this->assertEquals('asdf', $buffer->readLiteral(4));
        $this->assertEquals('', $buffer->readLiteral(4, 4)); // ensures we pad with noop
    }

    public function testReading(): void
    {
        $buffer = new Characters('foobar');

        $this->assertEquals('foo', $buffer->readLiteral(3)); // read start
        $this->assertEquals('oob', $buffer->readLiteral(3, 1)); // read with offset
        $this->assertEquals('foobar', $buffer->readLiteral(6)); // read all

        $this->assertEquals('f', $buffer->readOne()); // read just one

        $buffer->seek(3);
        $this->assertEquals('bar', $buffer->readLiteral(3)); // read truncated
    }


    public function testSlicing(): void
    {
        $this->assertEquals('oba', (new Characters('foobar'))->slice(2, 3)->readLiteral(3));
    }

    public function testValidFunction(): void
    {
        $buffer = new Characters('foobar');

        $this->assertTrue($buffer->valid());

        $buffer->read(6); // reads the whole string

        $this->assertTrue($buffer->valid());

        $buffer->seek(6);

        $this->assertFalse($buffer->valid());
    }

    #[Pure]
    private static function generator(): Generator
    {
        yield from str_split('foobar');
    }
}