<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\Tokens\Value;

use JetBrains\PhpStorm\ArrayShape;
use Judahnator\Lexer\Buffer\CharacterBuffer as Characters;
use Judahnator\Lexer\TokenMismatchException;
use judahnator\Lua\Lexer\Tokens\Value\StringToken;
use judahnator\Lua\Types\StringType;

/**
 * Class StringTokenTest
 * @package judahnator\Lua\Tests\Tokens
 * @covers \judahnator\Lua\Lexer\Tokens\Value\StringToken
 */
final class StringTokenTest extends ValueTokenTestCase
{
    #[ArrayShape([["callable", TokenMismatchException::class]])]
    public function provideConstructionExceptionData(): array
    {
        return [
            'strings must open with quotes' => [
                static fn () => new StringToken(new Characters('foo')),
                new TokenMismatchException('Strings must start with a single or double quote.'),
            ],
        ];
    }

    public function provideTokenData(): array
    {
        return [
            'strings may have single quotes' => [
                '\'foo\'', 'STRING', 5, '\'foo\'',
            ],
            'strings may have double quotes' => [
                '"foo bar"', 'STRING', 9, '"foo bar"',
            ]
        ];
    }

    public function provideTokenValueData(): array
    {
        return [
            'strings can be filled' => [
                '"foo"', new StringType('foo'),
            ],
            'strings can be empty' => [
                '""', new StringType(''),
            ],
        ];
    }
}