<?php declare(strict_types=1);

namespace judahnator\Lua;

use IterableCollection as Iter;
use Judahnator\Lexer\Contract\TokenInterface;
use judahnator\Lua\AST\Compiler;
use judahnator\Lua\AST\SignatureLexer;
use judahnator\Lua\Buffers\CharacterBuffer;
use Judahnator\Lexer\Token\Token;

final class Debug
{
    /**
     * Takes an input string and returns the JSON encoded AST.
     *
     * @return string
     */
    public static function ast(string $input, bool $single = false, int $flags = JSON_PRETTY_PRINT): string
    {
        $tokens = iterator_to_array((new Compiler())->compile((new Lexer())->tokenize(CharacterBuffer::fromString($input))));
        return json_encode($single ? $tokens[0] : $tokens, $flags);
    }

    public static function ast_tokens(string $input): string
    {
        $tokens = new SignatureLexer();
        return json_encode($tokens->tokenize(CharacterBuffer::fromString($input)), JSON_PRETTY_PRINT);
    }

    /**
     * Takes an input string and returns the lexer representation.
     *
     * @param string $input
     * @return string
     */
    public static function lexer(string $input): string
    {
       return Iter::wrap((new Lexer())->tokenize(CharacterBuffer::fromString($input)))
           ->map(static fn (TokenInterface $token): string => sprintf('%s, %s', $token->getName(), $token->getLiteral()))
           ->reduce(static fn (string $carry, string $line): string => $carry . $line . PHP_EOL, '');
    }
}