<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Identifier;

use Judahnator\Lexer\Buffer\TokenBuffer;
use judahnator\Lua\AST\AST;
use judahnator\Lua\AST\Compiler;
use judahnator\Lua\AST\Contracts\InfixIdentifier;
use judahnator\Lua\AST\Expression\Expression;
use judahnator\Lua\AST\Expression\OffsetExpression;
use judahnator\Lua\AST\SignatureLexer;
use judahnator\Lua\AST\SignatureParser;
use judahnator\Lua\Buffers\CharacterBuffer;

final class OffsetIdentifier implements InfixIdentifier
{
    public function matches(Expression $left, TokenBuffer $tokens, Compiler $compiler): AST
    {
        [$offset] = SignatureParser::matches('[%e]', $tokens);
        return new OffsetExpression($left, $offset);
    }
}