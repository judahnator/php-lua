<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\ASTs\Statement;

use judahnator\Lua\Parser;

/**
 * @covers \judahnator\Lua\AST\Statement\ForLoop
 */
final class ForLoopStatementTest extends StatementTestCase
{

    public function provideCountData(): array
    {
        return [
            ['for i = 10, 1, -1 do print(i) end', 14]
        ];
    }

    public function provideStringableData(): array
    {
        return [
            'basic for' => ['for i = 10, 1, -1 do print(i) end'],
        ];
    }

    public function provideJsonSerializeData(): array
    {
        return [
            [
                'for i = 5,1,-1 do print(i) end', <<<JSON
                {
                    "type": "ForLoop",
                    "token_count": 14,
                    "init": {
                        "type": "AssignmentExpression",
                        "token_count": 3,
                        "variable": {
                            "type": "VariableExpression",
                            "token_count": 1,
                            "token": "i"
                        },
                        "value": {
                            "type": "LiteralExpression",
                            "token_count": 1,
                            "token": "5"
                        }
                    },
                    "minmax": {
                        "type": "LiteralExpression",
                        "token_count": 1,
                        "token": "1"
                    },
                    "increment": {
                        "type": "LiteralExpression",
                        "token_count": 1,
                        "token": "-1"
                    },
                    "statements": [
                        {
                            "type": "ApplicationExpression",
                            "token_count": 4,
                            "method": {
                                "type": "VariableExpression",
                                "token_count": 1,
                                "token": "print"
                            },
                            "args": [
                                {
                                    "type": "VariableExpression",
                                    "token_count": 1,
                                    "token": "i"
                                }
                            ]
                        }
                    ]
                }
                JSON
            ],
        ];
    }

    public function providePrefixGenerationData(): array
    {
        return [
            'example statement' => [
                'for i = 10,1,-1 do print(i) end', <<<JSON
                {
                    "type": "ForLoop",
                    "token_count": 14,
                    "init": {
                        "type": "AssignmentExpression",
                        "token_count": 3,
                        "variable": {
                            "type": "VariableExpression",
                            "token_count": 1,
                            "token": "i"
                        },
                        "value": {
                            "type": "LiteralExpression",
                            "token_count": 1,
                            "token": "10"
                        }
                    },
                    "minmax": {
                        "type": "LiteralExpression",
                        "token_count": 1,
                        "token": "1"
                    },
                    "increment": {
                        "type": "LiteralExpression",
                        "token_count": 1,
                        "token": "-1"
                    },
                    "statements": [
                        {
                            "type": "ApplicationExpression",
                            "token_count": 4,
                            "method": {
                                "type": "VariableExpression",
                                "token_count": 1,
                                "token": "print"
                            },
                            "args": [
                                {
                                    "type": "VariableExpression",
                                    "token_count": 1,
                                    "token": "i"
                                }
                            ]
                        }
                    ]
                }
                JSON,
            ],
        ];
    }

    public function provideParseData(): array
    {
        return [
            'for loops do not execute when initial condition is false' => [
                'for i = 1, 1, -1 do print(i) end', <<<JSON
                    {}
                    JSON
            ],
            // todo multiple
        ];
    }

    public function testForLoop(): void
    {
        $parser = Parser::setup('for i = 5,1,-1 do print(i) end');
        $parser->run();
        $this->assertEquals(
            '54321',
            $parser->getEnv()->STDIO->out,
        );
    }
}