<?php declare(strict_types=1);

namespace judahnator\Lua\Lexer\Tokens;

use Judahnator\Lexer\Contract\BufferInterface;
use Judahnator\Lexer\Contract\TokenIdentifierInterface;
use Judahnator\Lexer\Contract\TokenInterface;
use Judahnator\Lexer\TokenMismatchException;
use judahnator\Lua\Lexer;

final class IdentityToken implements TokenIdentifierInterface
{

    public function __toString(): string
    {
        return 'ident';
    }
    
    public function matches(BufferInterface $characters): TokenInterface
    {
        $literal = '';
        for (
            $offset = 0;
            ctype_alnum(($char = $characters->readOne($offset))->getLiteral()) && !$char->isWhitespace();
            $offset++
        ) {
            if (is_numeric($char->getLiteral()) && $literal === '') {
                break;
            }
            $literal .= $char->getLiteral();
        }
        if ($literal === '') {
            throw new TokenMismatchException('Identity tokens must not start with whitespace, number, or be empty.');
        }
        return new \Judahnator\Lexer\Token\Token(Lexer\Keywords::IDENT->name, $literal);
    }
}