<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\Tokens\Value;

use JetBrains\PhpStorm\ArrayShape;
use Judahnator\Lexer\Buffer\CharacterBuffer as Characters;;
use judahnator\Lua\Lexer;
use judahnator\Lua\Lexer\Tokens\Value\Token;
use judahnator\Lua\Tests\Tokens\TokenTestCase;
use judahnator\Lua\Types\Type;

abstract class ValueTokenTestCase extends TokenTestCase
{
    abstract public function provideTokenValueData(): array;

    /**
     * @param string $tokenString
     * @param Type $value
     * @dataProvider provideTokenValueData
     */
    final public function testTokenValueData(string $tokenString, Type $value): void
    {
        $token = (new Lexer())->tokenize(new Characters($tokenString))->readOne();
        $this->assertEquals($token->getLiteral(), (string)$value);
    }
}