<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\ASTs;

use judahnator\Lua\Debug;

trait InfixTest
{
    abstract public function provideInfixGenerationData(): array;

    /**
     * @dataProvider provideInfixGenerationData
     * @param string $input
     * @param string $expected
     */
    public function testInfixGeneration(string $input, string $expected): void
    {
        $this->assertEquals($expected, Debug::ast($input, single: true));
    }
}