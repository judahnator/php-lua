<?php declare(strict_types=1);

namespace judahnator\Lua\Types;

use Stringable;

interface Type extends Stringable
{
    public function __toString(): string;

    public function getValue();

    public function toBool(): bool;
}