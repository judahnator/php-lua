<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\ASTs\Expression;

use judahnator\Lua\AST\Expression\VariableExpression;
use judahnator\Lua\Environment;
use judahnator\Lua\Exceptions\AST\MismatchException;
use judahnator\Lua\Lexer\Tokens\IdentityToken;
use judahnator\Lua\STDIO;
use judahnator\Lua\Tests\ASTs\PrefixTest;
use judahnator\Lua\Types\NilType;
use judahnator\Lua\Types\StringType;

/**
 * Class VariableExpressionTest
 * @package judahnator\Lua\Tests\ASTs\Expression
 * @covers \judahnator\Lua\AST\Expression\VariableExpression
 */
final class VariableExpressionTest extends ExpressionTestCase
{
    use PrefixTest;

    public function providePrefixGenerationData(): array
    {
        return [
            'basic variable' => [
                'foo', <<<JSON
                    {
                        "type": "VariableExpression",
                        "token_count": 1,
                        "token": "foo"
                    }
                    JSON
            ],
        ];
    }

    public function provideResultData(): array
    {
        return [
            'values are fetched from their environment' => [
                'foo', new StringType('bar'),
                Environment::fromArray(['foo' => new StringType('bar')], new STDIO())
            ],
            'values not in environment are nil' => [
                'foo', new NilType(),
            ],
        ];
    }

    public function provideCountData(): array
    {
        return [
            ['foo', 1],
            ['foobar', 1],
        ];
    }

    public function provideJsonSerializeData(): array
    {
        return [
            [
                'foo',
                <<<JSON
                {
                    "type": "VariableExpression",
                    "token_count": 1,
                    "token": "foo"
                }
                JSON,
            ]
        ];
    }

    public function provideStringableData(): array
    {
        return [
            'just returns literal' => ['literal'],
        ];
    }
}