<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\STD;

use judahnator\Lua\Exceptions\MethodSignatureException;
use judahnator\Lua\STD\Library\MinFunction;
use judahnator\Lua\Types\NumberType;
use judahnator\Lua\Types\StringType;

/**
 * @covers \judahnator\Lua\STD\Library\MinFunction
 * @covers \judahnator\Lua\STD\Library\Method
 */
final class MinFunctionTest extends MethodTestCase
{
    protected string $methodClass = MinFunction::class;

    public function provideConstructionErrorsData(): array
    {
        $exception = new MethodSignatureException('The "abs" function requires exactly two parameters of type number.');
        return [
            [$exception, new StringType('foo')],
            [$exception, new NumberType(0)],
            [$exception, new NumberType(0), new NumberType(1), new NumberType(2)],
        ];
    }

    public function provideInvocationData(): array
    {
        return [
            [0, new NumberType(0), new NumberType(1)],
            [10, new NumberType(20), new NumberType(10)],
        ];
    }
}