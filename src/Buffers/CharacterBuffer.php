<?php declare(strict_types=1);

namespace judahnator\Lua\Buffers;

use Generator;
use Judahnator\Lexer\Contract;
use Judahnator\Lexer\Token\Token;
use judahnator\Lua\Lexer;

class CharacterBuffer implements Contract\BufferInterface
{
    private array $buffer = [];

    /**
     * CharacterBuffer constructor.
     * @param Generator<string> $characters
     */
    public function __construct(private Generator $characters)
    {
    }

    public static function fromString(string $input): self
    {
        return new self(
            (static fn (): Generator => yield from str_split($input))()
        );
    }

    public function read(int $length = 1, int $offset = 0): array
    {
        $lookahead  = $length + $offset;
        while (count($this->buffer) < $lookahead && $this->characters->valid()) {
            $current = $this->characters->current();
            if ($current instanceof Token) {
                $this->buffer[] = $current->getLiteral();
            } else {
                $this->buffer[] = (string)$current;
            }
            $this->characters->next();
        }
        return array_pad(array_slice($this->buffer, $offset, $length), $length, ';');
    }

    public function readLiteral(int $length = 1, int $offset = 0): string
    {
        return implode($this->read($length, $offset));
    }

    public function seek(int $length): void
    {
        $this->buffer = array_slice($this->buffer, $length);
    }

    public function slice(int $offset, int $length = null): self
    {
        return new self((function () use ($offset, $length): Generator {
            while ($this->valid()) {
                yield $this->readOne($offset++);
                if (!is_null($length) && --$length === 0) {
                    break;
                }
            }
        })());
    }

    public function valid(): bool
    {
        return !empty($this->buffer) || $this->characters->valid();
    }

    public function readOne(int $offset = 0): ?Contract\TokenInterface
    {
        $literal = $this->readLiteral(offset: $offset);
        return new Token(self::class, $literal, Lexer::isWhitespaceCharacter($literal));
    }
}