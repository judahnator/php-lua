<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\ASTs\Statement;

use judahnator\Lua\AST\Statement\WhileLoop;
use judahnator\Lua\Exceptions\AST\MismatchException;

/**
 * Class WhileLoopStatementTest
 * @package judahnator\Lua\Tests\ASTs\Statement
 * @covers \judahnator\Lua\AST\Statement\WhileLoop
 */
final class WhileLoopStatementTest extends StatementTestCase
{
    public function providePrefixGenerationData(): array
    {
        return [
            'simple expression' => [
                'while true do foo = "bar" end', <<<JSON
                    {
                        "type": "WhileLoop",
                        "token_count": 7,
                        "condition": {
                            "type": "LiteralExpression",
                            "token_count": 1,
                            "token": "true"
                        },
                        "loop": [
                            {
                                "type": "AssignmentExpression",
                                "token_count": 3,
                                "variable": {
                                    "type": "VariableExpression",
                                    "token_count": 1,
                                    "token": "foo"
                                },
                                "value": {
                                    "type": "LiteralExpression",
                                    "token_count": 1,
                                    "token": "\"bar\""
                                }
                            }
                        ]
                    }
                    JSON
            ],
            'operator expression' => [
                'while infinity == infinity do infinity = infinity + 1 end', <<<JSON
                    {
                        "type": "WhileLoop",
                        "token_count": 11,
                        "condition": {
                            "type": "OperatorExpression",
                            "token_count": 3,
                            "operator": "==",
                            "lhs": {
                                "type": "VariableExpression",
                                "token_count": 1,
                                "token": "infinity"
                            },
                            "rhs": {
                                "type": "VariableExpression",
                                "token_count": 1,
                                "token": "infinity"
                            }
                        },
                        "loop": [
                            {
                                "type": "AssignmentExpression",
                                "token_count": 5,
                                "variable": {
                                    "type": "VariableExpression",
                                    "token_count": 1,
                                    "token": "infinity"
                                },
                                "value": {
                                    "type": "OperatorExpression",
                                    "token_count": 3,
                                    "operator": "+",
                                    "lhs": {
                                        "type": "VariableExpression",
                                        "token_count": 1,
                                        "token": "infinity"
                                    },
                                    "rhs": {
                                        "type": "LiteralExpression",
                                        "token_count": 1,
                                        "token": "1"
                                    }
                                }
                            }
                        ]
                    }
                    JSON
            ],
        ];
    }

    public function provideParseData(): array
    {
        return [
            'while loops do not execute when condition is false' => [
                'while false do hello = "world" end', <<<JSON
                    {}
                    JSON
            ],
            // todo multiple
        ];
    }

    public function provideCountData(): array
    {
        return [
            ['while false do return nil end', 6],
            ['while a > b do a = a + 1 end', 11],
        ];
    }

    public function provideJsonSerializeData(): array
    {
        return [
            [
                'while true do print("foo") end',
                <<<JSON
                {
                    "type": "WhileLoop",
                    "token_count": 8,
                    "condition": {
                        "type": "LiteralExpression",
                        "token_count": 1,
                        "token": "true"
                    },
                    "loop": [
                        {
                            "type": "ApplicationExpression",
                            "token_count": 4,
                            "method": {
                                "type": "VariableExpression",
                                "token_count": 1,
                                "token": "print"
                            },
                            "args": [
                                {
                                    "type": "LiteralExpression",
                                    "token_count": 1,
                                    "token": "\"foo\""
                                }
                            ]
                        }
                    ]
                }
                JSON
            ],
            [
                'while foo >= bar do foo = foo + 1 print(foo) end',
                <<<JSON
                {
                    "type": "WhileLoop",
                    "token_count": 15,
                    "condition": {
                        "type": "OperatorExpression",
                        "token_count": 3,
                        "operator": ">=",
                        "lhs": {
                            "type": "VariableExpression",
                            "token_count": 1,
                            "token": "foo"
                        },
                        "rhs": {
                            "type": "VariableExpression",
                            "token_count": 1,
                            "token": "bar"
                        }
                    },
                    "loop": [
                        {
                            "type": "AssignmentExpression",
                            "token_count": 5,
                            "variable": {
                                "type": "VariableExpression",
                                "token_count": 1,
                                "token": "foo"
                            },
                            "value": {
                                "type": "OperatorExpression",
                                "token_count": 3,
                                "operator": "+",
                                "lhs": {
                                    "type": "VariableExpression",
                                    "token_count": 1,
                                    "token": "foo"
                                },
                                "rhs": {
                                    "type": "LiteralExpression",
                                    "token_count": 1,
                                    "token": "1"
                                }
                            }
                        },
                        {
                            "type": "ApplicationExpression",
                            "token_count": 4,
                            "method": {
                                "type": "VariableExpression",
                                "token_count": 1,
                                "token": "print"
                            },
                            "args": [
                                {
                                    "type": "VariableExpression",
                                    "token_count": 1,
                                    "token": "foo"
                                }
                            ]
                        }
                    ]
                }
                JSON
            ],
        ];
    }

    public function provideStringableData(): array
    {
        return [
            'basic while' => ['while true do boogie() end'],
            'compound while' => ['while false do boogie() print("woo!") end']
        ];
    }
}