<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Expression;

use JetBrains\PhpStorm\Pure;
use judahnator\Lua\Environment;
use judahnator\Lua\Types\Type;

final class ParenthesisExpression extends Expression
{
    public function __construct(
        private Expression $expression
    ) {}

    public function __debugInfo(): array
    {
        return [
            'expression' => $this->expression,
        ];
    }

    public function __toString(): string
    {
        return "({$this->expression})";
    }

    public function count(): int
    {
        return 2 + count($this->expression);
    }

    public function getResult(Environment $variables): Type
    {
        return $this->expression->getResult($variables);
    }
}