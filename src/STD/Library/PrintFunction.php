<?php declare(strict_types=1);

namespace judahnator\Lua\STD\Library;

use judahnator\Lua\AST\Expression\Expression;
use judahnator\Lua\Environment;
use judahnator\Lua\Exceptions\MethodSignatureException;
use judahnator\Lua\Types\NilType;
use judahnator\Lua\Types\Type;

final class PrintFunction extends Method
{
    public static string $name = 'print';

    protected function call(Environment $environment, Type ...$args): Type
    {
        if (empty($args)) {
            throw new MethodSignatureException('The "print" function requires at least one parameter.');
        }
        foreach ($args as $argument) {
            $environment->STDIO->out((string)$argument->getValue());
        }
        return new NilType();
    }
}