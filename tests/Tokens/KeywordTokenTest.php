<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\Tokens;

use JetBrains\PhpStorm\ArrayShape;
use Judahnator\Lexer\Buffer\CharacterBuffer as Characters;
use Judahnator\Lexer\TokenMismatchException;
use judahnator\Lua\Lexer\Tokens\Keyword;
use judahnator\Lua\Lexer\Tokens\Token;

/**
 * Class KeywordTokenTest
 * @package judahnator\Lua\Tests\Tokens
 * @covers \judahnator\Lua\Lexer\Tokens\Keyword
 */
final class KeywordTokenTest extends TokenTestCase
{
    public function provideConstructionExceptionData(): array
    {
        $keywords = new Keyword();
        return [
            'asdf if not a keyword' => [
                static fn () => $keywords->matches(new Characters('asdf fdsa')),
                new TokenMismatchException('No matching keyword found.'),
            ],
            'keywords must be followed by whitespace' => [
                static fn() => $keywords->matches(new Characters('infinity')), // tests "in"
                new TokenMismatchException('No matching keyword found.'),
            ],
        ];
    }

    public function provideTokenData(): array
    {
        $list = [];
        foreach (Keyword::KEYWORDS as $literal => $keyword) {
            $list["{$literal} is a keyword"] = [
                "{$literal} bar baz",
                $keyword->name,
                strlen($literal),
                $literal,
            ];
        }
        return $list;
    }
}
