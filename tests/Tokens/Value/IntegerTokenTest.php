<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\Tokens\Value;

use JetBrains\PhpStorm\ArrayShape;
use Judahnator\Lexer\Buffer\CharacterBuffer as Characters;;
use Judahnator\Lexer\TokenMismatchException;
use judahnator\Lua\Lexer\Tokens\Value\IntegerToken;
use judahnator\Lua\Lexer\Tokens\Token;
use judahnator\Lua\Types\NumberType;
use judahnator\Lua\Types\Type;

/**
 * Class IntegerTokenTest
 * @package judahnator\Lua\Tests\Tokens
 * @covers \judahnator\Lua\Lexer\Tokens\Value\IntegerToken
 * @covers \judahnator\Lua\Types\NumberType::__construct
 */
final class IntegerTokenTest extends ValueTokenTestCase
{
    #[ArrayShape([["callable", TokenMismatchException::class]])]
    public function provideConstructionExceptionData(): array
    {
        return [
            'integers may not bbe empty' => [
                static fn () => new IntegerToken(new Characters('')),
                new TokenMismatchException("Integers may not be empty."),
            ],
            'integers may not be empty 2' => [
                static fn () => new IntegerToken(new Characters('asdf')),
                new TokenMismatchException("Integers may not be empty."),
            ],
            'hyphens may not have whitespace after' => [
                static fn () => new IntegerToken(new Characters('-')),
                new TokenMismatchException("Integers may not contain only \"-\".")
            ],
        ];
    }

    public function provideTokenData(): array
    {
        return [
            'integers split on space' => [
                '123 456', 'INT', 3, '123'
            ],
            'integers can have many digits' => [
                '8675309 asdf', 'INT', 7, '8675309'
            ],
            'integers stop on semicolon' => [
                '123; 456;', 'INT', 3, '123'
            ]
        ];
    }

    public function provideTokenValueData(): array
    {
        return [
            'positive numbers work' => [
                '123', new NumberType(123),
            ],
            'negative numbers work' => [
                '-234', new NumberType(-234),
            ],
        ];
    }
}