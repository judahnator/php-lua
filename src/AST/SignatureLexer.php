<?php declare(strict_types=1);

namespace judahnator\Lua\AST;

use JetBrains\PhpStorm\Pure;
use Judahnator\Lexer\Buffer\TokenBuffer;
use Judahnator\Lexer\Contract\{BufferInterface, DictionaryInterface, LexerInterface};
use Judahnator\Lexer\Dictionary;
use Judahnator\Lexer\Lexer;
use Judahnator\Lexer\Token\ConstantTokenIdentifier;
use Judahnator\Lexer\Token\RepeatingTokenIdentifier;
use Judahnator\Lexer\Token\VariableTokenIdentifier;
use judahnator\Lua\Lexer as LuaLexer;
use judahnator\Lua\Lexer\Tokens\Keyword;

final class SignatureLexer implements LexerInterface
{
    public readonly DictionaryInterface $dictionary;

    public function __construct()
    {
        $this->dictionary = new Dictionary(
            new Keyword(),

            new VariableTokenIdentifier([
                // High level
                'AST' => '%a',
                'EXPRESSION' => '%e',
                'STATEMENT' => '%s',
                'IDENTITY' => '%i',

                // Abstractions
                'VALUE' => '%v',
                'REPEATING' => '...',

                // Low Level
                'PAREN_OPEN' => '(',
                'PAREN_CLOSE' => ')',
                'BRACKET_OPEN' => '[',
                'BRACKET_CLOSE' => ']',
                'SQ_BRACKET_OPEN' => '{',
                'SQ_BRACKET_CLOSE' => '}',
                'COLON' => ':',
                'COMMA' => ',',
                'ASSIGN' => '=',
            ]),

            // Repeating Ast
            new RepeatingTokenIdentifier(
                new ConstantTokenIdentifier('AST_LIST', '%A')
            ),

            // Repeating Statement
            new RepeatingTokenIdentifier(
                new ConstantTokenIdentifier('EXPRESSION_LIST', '%E')
            ),
        );
    }

    #[Pure] public static function isWhitespaceCharacter(string $char): bool
    {
        return LuaLexer::isWhitespaceCharacter($char);
    }

    public function tokenize(BufferInterface $characters): TokenBuffer
    {
        return (new Lexer($this->dictionary))->tokenize($characters);
    }
}