<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\ASTs\Expression;

use judahnator\Lua\AST\AST;
use judahnator\Lua\AST\Expression\Expression;
use judahnator\Lua\Environment;
use judahnator\Lua\STDIO;
use judahnator\Lua\Tests\ASTs\AbstractSyntaxTreeTestCase;
use judahnator\Lua\Tests\ASTs\InfixTest;
use judahnator\Lua\Tests\ASTs\PrefixTest;
use judahnator\Lua\Types\Type;
use judahnator\TraitAware\TraitAware;
use RuntimeException;

abstract class ExpressionTestCase extends AbstractSyntaxTreeTestCase
{
    use TraitAware;

    final public static function setUpBeforeClass(): void
    {
        if (empty(array_intersect([InfixTest::class, PrefixTest::class], self::getTraits()))) {
            throw new RuntimeException('An Expression Test must implement the InfixTest, PrefixTest, or both.');
        }
    }

    abstract public function provideResultData(): array;

    /**
     * @dataProvider provideResultData
     * @param string $input
     * @param Type $result
     * @param Environment|null $environment
     */
    final public function testGetResultFunction(string $input, Type $result, Environment $environment = null): void
    {
        if (!($ast = self::express($input)->current()) instanceof Expression) {
            $this->fail('The provided input does not represent an expression!');
        }
        $this->assertEquals(
            $ast->getResult(
                $environment ?: new Environment(IO: new STDIO(STDIO::DRIVER_MEMORY))
            ),
            $result
        );
    }
}