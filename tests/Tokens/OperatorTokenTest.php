<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\Tokens;

use Judahnator\Lexer\Buffer\CharacterBuffer as Characters;;
use Judahnator\Lexer\TokenMismatchException;
use judahnator\Lua\Lexer\Tokens\Operator as Token;
use judahnator\Lua\Operators\Operator;

/**
 * Class OperatorTokenTest
 * @package judahnator\Lua\Tests\Tokens
 * @covers \judahnator\Lua\Lexer\Tokens\Operator
 * @uses \judahnator\Lua\Operators\Operator
 */
final class OperatorTokenTest extends TokenTestCase
{
    public function provideTokenOperatorData(): array
    {
        $op = static fn (string $input): Token => new Token(new Characters($input));
        return [
            'plus sign is addition' => [
                $op('+'), Operator::get('+'),
            ],
            'minus sign is subtraction' => [
                $op('-'), Operator::get('-'),
            ],
            'astrix is multiplication' => [
                $op('*'), Operator::get('*'),
            ],
            'forward slash is division' => [
                $op('/'), Operator::get('/'),
            ],
            'percent is modulus' => [
                $op('%'), Operator::get('%'),
            ],
            'caret is power' => [
                $op('^'), Operator::get('^'),
            ],
            'double equals is "is"' => [
                $op('=='), Operator::get('=='),
            ],
            'tilda equals is "is not"' => [
                $op('~='), Operator::get('~='),
            ],
            '> is greater than' => [
                $op('>'), Operator::get('>'),
            ],
            '>= is greater than equal' => [
                $op('>='), Operator::get('>='),
            ],
            '< is less than' => [
                $op('<'), Operator::get('<'),
            ],
            '<= is less than equal' => [
                $op('<='), Operator::get('<='),
            ],
        ];
    }

    public function provideConstructionExceptionData(): array
    {
        $exception = new TokenMismatchException('The provided characters do not represent an operator.');
        return [
            'literals are not tokens' => [
                static fn () => new Token(new Characters('foo')),
                $exception,
            ],
            'semicolon are not operators' => [
                static fn () => new Token(new Characters(';')),
                $exception,
            ],
        ];
    }

    public function provideTokenData(): array
    {
        return [
            'plus sign is addition' => [
                '+', 'ADD', 1, '+',
            ],
            'minus sign is subtraction' => [
                '-', 'SUB', 1, '-',
            ],
            'astrix is multiplication' => [
                '*', 'MUL', 1, '*',
            ],
            'forward slash is division' => [
                '/', 'DIV', 1, '/',
            ],
            'percent is modulus' => [
                '%', 'MOD', 1, '%',
            ],
            'caret is power' => [
                '^', 'POW', 1, '^',
            ],
            'double equals is "is"' => [
                '==', 'IS', 2, '==',
            ],
            'tilda equals is "is not"' => [
                '~=', 'ISNOT', 2, '~=',
            ]
        ];
    }

    /**
     * @param Token $token
     * @param Operator $value
     * @dataProvider provideTokenOperatorData
     */
    final public function testTokenOperatorData(Token $token, Operator $value): void
    {
        $this->assertEquals($token->getOperator(), $value);
    }
}