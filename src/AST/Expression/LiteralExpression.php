<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Expression;

use JetBrains\PhpStorm\Immutable;
use JetBrains\PhpStorm\Pure;
use Judahnator\Lexer\Buffer\TokenBuffer;
use judahnator\Lua\AST\Compiler;
use judahnator\Lua\Environment;
use judahnator\Lua\Exceptions\AST\MismatchException;
use judahnator\Lua\Lexer\Tokens\Value\Token as ValueToken;
use judahnator\Lua\Types\Type;

#[Immutable]
final class LiteralExpression extends Expression
{
    public function __construct(
        private ValueToken $token,
    ) {}

    public function __debugInfo(): array
    {
        return [
            'token' => $this->token->getLiteral(),
        ];
    }

    public function __toString(): string
    {
        return $this->token->getLiteral();
    }

    public static function compilePrefix(TokenBuffer $tokens, Compiler $compiler): self
    {
        if (!($token = $tokens->readOne()) instanceof ValueToken) {
            throw new MismatchException('The provided token does not represent a value.');
        }
        return new self($token);
    }

    /**
     * Returns the number of tokens involved in this AST node.
     * @return int
     */
    #[Pure] public function count(): int
    {
        return 1;
    }

    /**
     * Returns the raw scalar type.
     * @param Environment $variables
     * @return Type
     */
    #[Pure] public function getResult(Environment $variables): Type
    {
        return $this->token->getValue();
    }
}