<?php declare(strict_types=1);

namespace judahnator\Lua\Lexer\Tokens;

use Judahnator\Lexer\Contract\BufferInterface;
use Judahnator\Lexer\Contract\ValueTokenIdentifierInterface;
use Judahnator\Lexer\Contract\ValueTokenInterface;
use Judahnator\Lexer\TokenMismatchException;
use judahnator\Lua\Lexer\Tokens\Value\BooleanToken;
use judahnator\Lua\Lexer\Tokens\Value\FloatToken;
use judahnator\Lua\Lexer\Tokens\Value\IntegerToken;
use judahnator\Lua\Lexer\Tokens\Value\NilToken;
use judahnator\Lua\Lexer\Tokens\Value\StringToken;

final class ValueToken implements ValueTokenIdentifierInterface
{
    private static array $valueTokens = [
        BooleanToken::class,
        FloatToken::class,
        IntegerToken::class,
        NilToken::class,
        StringToken::class,
    ];

    public function __toString(): string
    {
        return '[bool|float|int|nil|string]';
    }

    public function matches(BufferInterface $characters): ValueTokenInterface
    {
        foreach (self::$valueTokens as $valueToken) {
            try {
                return new $valueToken($characters);
            } catch (TokenMismatchException) {
                continue;
            }
        }
        throw new TokenMismatchException();
    }
}