<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\Tokens\Value;

use JetBrains\PhpStorm\ArrayShape;
use Judahnator\Lexer\Buffer\CharacterBuffer as Characters;;
use Judahnator\Lexer\TokenMismatchException;
use judahnator\Lua\Lexer\Tokens\Value\FloatToken;
use judahnator\Lua\Lexer\Tokens\Token;
use judahnator\Lua\Types\NumberType;
use judahnator\Lua\Types\Type;

/**
 * Class FloatTokenTest
 * @package judahnator\Lua\Tests\Tokens
 * @covers \judahnator\Lua\Lexer\Tokens\Value\FloatToken
 * @covers \judahnator\Lua\Types\NumberType::__construct
 */
final class FloatTokenTest extends ValueTokenTestCase
{
    #[ArrayShape([["callable", TokenMismatchException::class]])]
    public function provideConstructionExceptionData(): array
    {
        return [
            'floats may not start with decimal' => [
                static fn () => new FloatToken(new Characters('.')),
                new TokenMismatchException('Floats may not start with a decimal.'),
            ],
            'floats cannot have multiple decimal places' => [
                static fn () => new FloatToken(new Characters('0.1.2')),
                new TokenMismatchException('Floats may not have more than one decimal place.')
            ],
            'floats may not have numbers' => [
                static fn () => new FloatToken(new Characters('12.3a')),
                new TokenMismatchException('Integers may not contain the character "a".'),
            ],
            'floats must have a decimal place' => [
                static fn () => new FloatToken(new Characters('123')),
                new TokenMismatchException('Floats must have one decimal place.'),
            ],
            'floats cannot end with decimal' => [
                static fn () => new FloatToken(new Characters('1.')),
                new TokenMismatchException('Floats must not end with a decimal.'),
            ],
        ];
    }

    public function provideTokenData(): array
    {
        return [
            'floats can be short' => [
                '0.0', 'FLOAT', 3, '0.0',
            ],
            'floats can be long' => [
                '8675309.1234567', 'FLOAT', 15, '8675309.1234567',
            ],
            'floats are split by whitespace' => [
                '12.34 56', 'FLOAT', 5, '12.34',
            ],
            'floats are split by eof' => [
                '56.78', 'FLOAT', 5, '56.78',
            ],
            'floats are split by semicolon' => [
                '12.34;56.78', 'FLOAT', 5, '12.34',
            ],
        ];
    }

    /**
     * @covers \judahnator\Lua\Types\NumberType::__construct
     * @return array[]
     */
    public function provideTokenValueData(): array
    {
        return [
            'positive numbers work' => [
                '1.23', new NumberType(1.23),
            ],
            'negative numbers work' => [
                '-2.34', new NumberType(-2.34),
            ],
        ];
    }
}