<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\Tokens;

use JetBrains\PhpStorm\ArrayShape;
use Judahnator\Lexer\Buffer\CharacterBuffer as Characters;;
use Judahnator\Lexer\TokenMismatchException;
use judahnator\Lua\Lexer\Tokens\IdentityToken;
use judahnator\Lua\Lexer\Tokens\Token;

/**
 * Class IdentityTokenTest
 * @package judahnator\Lua\Tests\Tokens
 * @covers \judahnator\Lua\Lexer\Tokens\IdentityToken
 */
final class IdentityTokenTest extends TokenTestCase
{
    #[ArrayShape([["callable", TokenMismatchException::class]])]
    public function provideConstructionExceptionData(): array
    {
        $identity = new IdentityToken();
        $exception = new TokenMismatchException('Identity tokens must not start with whitespace, number, or be empty.');
        return [
            'identity tokens may not start with whitespace' => [
                static fn() => $identity->matches(new Characters(' foo ')),
                $exception,
            ],
            'identity tokens may not be empty' => [
                static fn() => $identity->matches(new Characters('')),
                $exception,
            ],
            'identity tokens may not start with a number' => [
                static fn() => $identity->matches(new Characters('1foo')),
                $exception,
            ]
        ];
    }

    public function provideTokenData(): array
    {
        return [
            'identity tokens may be anything alpha/num' => [
                'foo123+bar456', 'IDENT', 6, 'foo123',
            ],
            'identity tokens are broken by whitespace' => [
                'foo bar', 'IDENT', 3, 'foo',
            ],
            'identity tokens are broken by noop' => [
                'bing;baz;', 'IDENT', 4, 'bing',
            ],
        ];
    }
}