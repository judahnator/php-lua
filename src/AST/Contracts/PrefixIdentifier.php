<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Contracts;

use Judahnator\Lexer\Buffer\TokenBuffer;
use judahnator\Lua\AST\AST;
use judahnator\Lua\AST\Compiler;

interface PrefixIdentifier
{
    public function matches(TokenBuffer $tokens, Compiler $compiler): AST;
}