<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Expression;

use JetBrains\PhpStorm\Immutable;
use JetBrains\PhpStorm\Pure;
use Judahnator\Lexer\Contract\TokenInterface;
use judahnator\Lua\AST\Contracts\Writable;
use judahnator\Lua\Environment;
use judahnator\Lua\Types\LambdaType;
use judahnator\Lua\Types\NilType;
use judahnator\Lua\Types\Type;

#[Immutable]
final class VariableExpression extends Expression implements Writable
{
    public function __construct(
        public TokenInterface $token,
    ) {

    }

    public function __debugInfo(): array
    {
        return [
            'token' => $this->token->getLiteral(),
        ];
    }

    public function __toString(): string
    {
        return $this->token->getLiteral();
    }

    /**
     * Returns the number of tokens involved in this AST node.
     * @return int
     */
    public function count(): int
    {
        return 1;
    }

    /**
     * Returns the raw scalar type.
     * @param Environment $variables
     * @return Type
     */
    public function getResult(Environment $variables): Type
    {
        return $variables->offsetGet($this->token->getLiteral()) ?: new NilType();
    }

    public function writeValue(Environment $variables, Type $newValue): void
    {
        if ($newValue instanceof LambdaType) {
            $newValue->scope = $variables;
        }
        $variables->offsetSet($this->token->getLiteral(), $newValue);
    }
}