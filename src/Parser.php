<?php declare(strict_types=1);

namespace judahnator\Lua;

use DateInterval;
use DateTime;
use JetBrains\PhpStorm\Pure;
use judahnator\Lua\AST as AST;
use judahnator\Lua\Exceptions\TimeoutException;
use judahnator\Lua\STD\Library\AbsFunction;
use judahnator\Lua\STD\Library\MaxFunction;
use judahnator\Lua\STD\Library\Method;
use judahnator\Lua\STD\Library\MinFunction;
use judahnator\Lua\STD\Library\PrintFunction;
use judahnator\Lua\Types\LambdaType;
use judahnator\Lua\Types\NumberType;
use judahnator\Lua\Types\Type;

final class Parser
{
    /**
     * @var array<class-string<Method>> $SPL
     */
    private static array $SPL = [
        AbsFunction::class,
        PrintFunction::class,
        MaxFunction::class,
        MinFunction::class,
    ];

    private ?DateTime $executionTimeout = null;

    /**
     * Parser constructor.
     * @param Environment $env
     * @param iterable<AST\AST> $nodes
     * @param ?DateInterval $timeLimit
     */
    #[Pure] public function __construct(private Environment $env, private iterable $nodes, private ?DateInterval $timeLimit = null)
    {
    }

    public function bootstrap(): void
    {
        foreach (self::$SPL as $class) {
            $method = new $class($this->env);
            $this->env->offsetSet($method::$name, LambdaType::fromCallable($method::$name, $method));
        }
    }

    public function getEnv(): Environment
    {
        return $this->env;
    }

    public static function setup(string $script, ?STDIO $IO = null, ?DateInterval $timeLimit = null): self
    {
        return new self(
            new Environment(IO: $IO ?: new STDIO(STDIO::DRIVER_MEMORY)),
            AST\Compiler::compileString($script),
            $timeLimit,
        );
    }

    public function run(): Type
    {
        if ($this->timeLimit !== null) {
            $this->executionTimeout = (new DateTime())->add($this->timeLimit);
        }
        $this->bootstrap();
        foreach ($this->nodes as $ast) {
            if (($result = $this->execute($ast)) instanceof Type) {
                return $result;
            }
        }

        return new NumberType(0);
    }

    private function execute(AST\AST $ast): ?Type
    {
        if ($this->executionTimeout !== null && (new DateTime()) > $this->executionTimeout) {
            throw new TimeoutException("Timeout exceeded. Execution halted at: \"$ast\"");
        }

        if ($ast instanceof AST\Expression\AssignmentExpression) { // assignment with expression
            $ast->variable->writeValue($this->env, $ast->getResult($this->env));
            return null;
        }

        if ($ast instanceof AST\Expression\ReturnExpression) {
            return $ast->getResult($this->env);
        }

        if ($ast instanceof AST\Expression\FunctionExpression) {
            $ast->lambda->scope = $this->env; // function scope is bound at definition
            // only define in global namespace if named
            $this->env->offsetSet($ast->lambda->name, $ast->lambda);
            return null;
        }

        if ($ast instanceof AST\Expression\Expression) { // expression without assignment
            $ast->getResult($this->env);
            return null;
        }

        if ($ast instanceof AST\Statement\Statement) { // statement
            foreach ($ast->parse($this->env) as $statement) {
                if (($result = $this->execute($statement)) instanceof Type) {
                    return $result;
                }
            }
            return null;
        }

        throw new \Exception('TODO - parser exception');
    }
}
