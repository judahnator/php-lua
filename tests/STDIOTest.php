<?php declare(strict_types=1);

namespace judahnator\Lua\Tests;

use InvalidArgumentException;
use judahnator\Lua\STDIO;
use PHPUnit\Framework\TestCase;

/**
 * @todo test stdio driver
 * @covers \judahnator\Lua\STDIO
 */
final class STDIOTest extends TestCase
{
    public function testConstructionException(): void
    {
        $this->expectExceptionObject(new InvalidArgumentException('Invalid driver argument provided.'));
        new STDIO(99);
    }

    public function testMemory(): void
    {
        $io = new STDIO(STDIO::DRIVER_MEMORY);
        $io->out('foo');
        $io->out('bar');
        $io->err('bing');
        $io->err('baz');

        $this->assertEquals('foobar', $io->out);
        $this->assertEquals('bingbaz', $io->err);
    }
}
