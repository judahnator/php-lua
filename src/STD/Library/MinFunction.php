<?php declare(strict_types=1);

namespace judahnator\Lua\STD\Library;

use judahnator\Lua\Environment;
use judahnator\Lua\Exceptions\MethodSignatureException;
use judahnator\Lua\Types\NumberType;
use judahnator\Lua\Types\Type;

final class MinFunction extends Method
{
    public static string $name = 'min';

    protected function call(Environment $environment, Type ...$args): Type
    {
        if (count($args) !== 2 || !($lhs = $args[0]) instanceof NumberType || !($rhs = $args[1]) instanceof NumberType) {
            throw new MethodSignatureException('The "abs" function requires exactly two parameters of type number.');
        }
        return new NumberType(min($lhs->getValue(), $rhs->getValue()));
    }
}