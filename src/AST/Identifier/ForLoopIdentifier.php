<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Identifier;

use Judahnator\Lexer\Buffer\TokenBuffer;
use judahnator\Lua\AST\AST;
use judahnator\Lua\AST\Compiler;
use judahnator\Lua\AST\Contracts\PrefixIdentifier;
use judahnator\Lua\AST\SignatureParser;
use judahnator\Lua\AST\Statement\ForLoop;

final class ForLoopIdentifier implements PrefixIdentifier
{
    public function matches(TokenBuffer $tokens, Compiler $compiler): AST
    {
        return new ForLoop(...SignatureParser::matches(
            'for %e, %e, %e do %A end',
            $tokens,
        ));
    }
}