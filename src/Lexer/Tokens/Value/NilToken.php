<?php declare(strict_types=1);

namespace judahnator\Lua\Lexer\Tokens\Value;

use Judahnator\Lexer\Contract\BufferInterface;
use Judahnator\Lexer\TokenMismatchException;
use judahnator\Lua\Lexer;
use judahnator\Lua\Lexer\Keywords;
use judahnator\Lua\Types\NilType;
use judahnator\Lua\Types\Type;

final class NilToken implements Token
{
    public function __construct(BufferInterface $characters)
    {
        $tail = $characters->readOne(3);
        if (
            $characters->readLiteral(3) !== 'nil' || // not 'nil'
            !$tail->isWhitespace() ||
            ctype_alpha($tail->getLiteral()) // has trailing
        ) {
            throw new TokenMismatchException('Nil tokens must be exactly nil');
        }
    }

    public function getName(): string
    {
        return Lexer\Keywords::NIL->name;
    }

    public function getValue(): Type
    {
        return new NilType();
    }

    public function count(): int
    {
        return 3;
    }

    public function getLiteral(): string
    {
        return 'nil';
    }

    public function isWhitespace(): bool
    {
        return false;
    }
}