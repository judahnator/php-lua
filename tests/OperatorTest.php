<?php declare(strict_types=1);

namespace judahnator\Lua\Tests;

use judahnator\Lua\Operators\Operator;
use judahnator\Lua\Types\NumberType;
use PHPUnit\Framework\TestCase;

/**
 * @covers \judahnator\Lua\Operators\Operator
 */
final class OperatorTest extends TestCase
{
    public function testOperators(): void
    {
        $operators = [
            ['+', 10, 20, 30],
            ['-', 30, 20, 10],
            ['*', 2, 3, 6],
            ['/', 6, 2, 3],
            ['%', 10, 4, 2],
            ['^', 2, 8, 256],
            ['==', 10, 10, true],
            ['~=', 10, 10, false],
            ['>', 10, 10, false],
            ['>=', 10, 10, true],
            ['<', 10, 10, false],
            ['<=', 10, 10, true],
        ];

        foreach ($operators as [$symbol, $lhs, $rhs, $expected]) {
            $this->assertEquals(
                $expected,
                Operator::get($symbol)
                    ->parse(new NumberType($lhs), new NumberType($rhs))
                    ->getValue()
            );
        }
    }
}