<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\ASTs;

use judahnator\Lua\Debug;

trait PrefixTest
{
    abstract public function providePrefixGenerationData(): array;

    /**
     * @dataProvider providePrefixGenerationData
     * @param string $input
     * @param string $expected
     */
    public function testPrefixGeneration(string $input, string $expected): void
    {
        $this->assertEquals($expected, Debug::ast($input, true));
    }
}