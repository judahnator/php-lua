<?php declare(strict_types=1);

namespace judahnator\Lua\Lexer;

enum Keywords
{
    // types
    case BOOL;
    case FLOAT;
    case INT;
    case LIST;
    case MAP;
    case NIL;
    case STRING;

    // misc
    case ASSIGN;
    case COMMA;
    case COLON;

    // identity special case
    case IDENT;

    // keywords
    case KEYWORD_FUNCTION;
    case KEYWORD_ELSEIF;
    case KEYWORD_REPEAT;
    case KEYWORD_RETURN;
    case KEYWORD_BREAK;
    case KEYWORD_LOCAL;
    case KEYWORD_UNTIL;
    case KEYWORD_WHILE;
    case KEYWORD_ELSE;
    case KEYWORD_THEN;
    case KEYWORD_AND;
    case KEYWORD_END;
    case KEYWORD_FOR;
    case KEYWORD_NOT;
    case KEYWORD_DO;
    case KEYWORD_IF;
    case KEYWORD_IN;
    case KEYWORD_OR;

    // operators
    case ADD;
    case SUB;
    case MUL;
    case DIV;
    case MOD;
    case POW;
    case IS;
    case ISNOT;
    case GTE;
    case LTE;
    case GT;
    case LT;

    // parenthesis
    case PAREN_OPEN;
    case PAREN_CLOSE;

    // brackets
    case BRACKET_OPEN;
    case BRACKET_CLOSE;
}