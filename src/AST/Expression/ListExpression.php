<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Expression;

use judahnator\Lua\Environment;
use judahnator\Lua\Types\ListType;
use judahnator\Lua\Types\Type;

final class ListExpression extends Expression
{
    private array $contents;

    public function __construct(Expression ...$contents) 
    {
        $this->contents = $contents;
    }

    public function __debugInfo(): array
    {
        return [
            'contents' => $this->contents,
        ];
    }

    public function __toString(): string
    {
        return '[' . implode(' ', $this->contents) . ']';
    }

    public function count(): int
    {
        return 2 + self::countList(...$this->contents);
    }

    public function getResult(Environment $variables): Type
    {
        return new ListType(...array_map(
            static fn (Expression $item): Type => $item->getResult($variables),
            $this->contents,
        ));
    }
}