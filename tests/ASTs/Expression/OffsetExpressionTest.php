<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\ASTs\Expression;

use judahnator\Lua\AST\Expression\OffsetExpression;
use judahnator\Lua\Environment;
use judahnator\Lua\STDIO;
use judahnator\Lua\Tests\ASTs\InfixTest;
use judahnator\Lua\Types\NilType;
use judahnator\Lua\Types\StringType;
use judahnator\Lua\Types\TableType;

/**
 * @covers \judahnator\Lua\AST\Expression\OffsetExpression
 */
final class OffsetExpressionTest extends ExpressionTestCase
{
    use InfixTest;

    public function provideResultData(): array
    {
        return [
            'values are fetched from their environment' => [
                'foo["bar"]',
                new StringType('baz'),
                Environment::fromArray(
                    ['foo' => new TableType(['bar' => new StringType('baz')])],
                    new STDIO(),
                ),
            ],
            'values not in environment are nil' => [
                'foo["bar"]',
                new NilType(),
                Environment::fromArray(
                    ['foo' => new TableType([])],
                    new STDIO(),
                ),
            ],
        ];
    }

    public function provideCountData(): array
    {
        return [
            ['foo[bar]', 4],
            ['foo[bar["baz"]]', 7],
        ];
    }

    public function provideJsonSerializeData(): array
    {
        return [
            [
                'foo[bar]',
                <<<JSON
                {
                    "type": "OffsetExpression",
                    "token_count": 4,
                    "parent": {
                        "type": "VariableExpression",
                        "token_count": 1,
                        "token": "foo"
                    },
                    "offset": {
                        "type": "VariableExpression",
                        "token_count": 1,
                        "token": "bar"
                    }
                }
                JSON,
            ],
            [
                'foo["bar"]["baz"]',
                <<<JSON
                {
                    "type": "OffsetExpression",
                    "token_count": 7,
                    "parent": {
                        "type": "OffsetExpression",
                        "token_count": 4,
                        "parent": {
                            "type": "VariableExpression",
                            "token_count": 1,
                            "token": "foo"
                        },
                        "offset": {
                            "type": "LiteralExpression",
                            "token_count": 1,
                            "token": "\"bar\""
                        }
                    },
                    "offset": {
                        "type": "LiteralExpression",
                        "token_count": 1,
                        "token": "\"baz\""
                    }
                }
                JSON,
            ],
        ];
    }

    public function provideInfixGenerationData(): array
    {
        return [
            [
                'foo[bar]',
                <<<JSON
                {
                    "type": "OffsetExpression",
                    "token_count": 4,
                    "parent": {
                        "type": "VariableExpression",
                        "token_count": 1,
                        "token": "foo"
                    },
                    "offset": {
                        "type": "VariableExpression",
                        "token_count": 1,
                        "token": "bar"
                    }
                }
                JSON,
            ],
            [
                'foo["bar"]["baz"]',
                <<<JSON
                {
                    "type": "OffsetExpression",
                    "token_count": 7,
                    "parent": {
                        "type": "OffsetExpression",
                        "token_count": 4,
                        "parent": {
                            "type": "VariableExpression",
                            "token_count": 1,
                            "token": "foo"
                        },
                        "offset": {
                            "type": "LiteralExpression",
                            "token_count": 1,
                            "token": "\"bar\""
                        }
                    },
                    "offset": {
                        "type": "LiteralExpression",
                        "token_count": 1,
                        "token": "\"baz\""
                    }
                }
                JSON,
            ],
            [
                'foo[bar["baz"]]',
                <<<JSON
                {
                    "type": "OffsetExpression",
                    "token_count": 7,
                    "parent": {
                        "type": "VariableExpression",
                        "token_count": 1,
                        "token": "foo"
                    },
                    "offset": {
                        "type": "OffsetExpression",
                        "token_count": 4,
                        "parent": {
                            "type": "VariableExpression",
                            "token_count": 1,
                            "token": "bar"
                        },
                        "offset": {
                            "type": "LiteralExpression",
                            "token_count": 1,
                            "token": "\"baz\""
                        }
                    }
                }
                JSON,
            ],
        ];
    }

    public function provideStringableData(): array
    {
        return [
            'basic offset' => ['foo[bar]'],
            'nested offset' => ['foo[bar[baz]]'],
            'literal offset' => ['foo["bar"]']
        ];
    }
}