<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\ASTs\Expression;

use judahnator\Lua\Tests\ASTs\PrefixTest;
use judahnator\Lua\Types\NilType;
use judahnator\Lua\Types\StringType;

/**
 * @covers \judahnator\Lua\AST\Expression\ParenthesisExpression
 */
final class ParenthesisExpressionTest extends ExpressionTestCase
{
    use PrefixTest;

    public function provideCountData(): array
    {
        return [
            ['(nil)', 3],
            ['("foo")', 3],
            ['(foo < bar)', 5],
        ];
    }

    public function provideStringableData(): array
    {
        return [
            'parenthesis around nil' => ['(nil)'],
            'parenthesis around number' => ['(42)'],
            'parenthesis around string' => ['("foo")'],
        ];
    }

    public function provideJsonSerializeData(): array
    {
        return [
            [
                '(nil)',
                <<<JSON
                {
                    "type": "ParenthesisExpression",
                    "token_count": 3,
                    "expression": {
                        "type": "VariableExpression",
                        "token_count": 1,
                        "token": "nil"
                    }
                }
                JSON,
            ],
            [
                '("baz")',
                <<<JSON
                {
                    "type": "ParenthesisExpression",
                    "token_count": 3,
                    "expression": {
                        "type": "LiteralExpression",
                        "token_count": 1,
                        "token": "\"baz\""
                    }
                }
                JSON,
            ],
        ];
    }

    public function provideResultData(): array
    {
        return [
            ['(nil)', new NilType()],
            ['("foo")', new StringType("foo")],
        ];
    }

    public function providePrefixGenerationData(): array
    {
        return [
            'nil in parenthesis' => [
                '(nil)', <<<JSON
                {
                    "type": "ParenthesisExpression",
                    "token_count": 3,
                    "expression": {
                        "type": "VariableExpression",
                        "token_count": 1,
                        "token": "nil"
                    }
                }
                JSON,
            ],
            'expression in parenthesis' => [
                '(foo < bar)', <<<JSON
                {
                    "type": "ParenthesisExpression",
                    "token_count": 5,
                    "expression": {
                        "type": "OperatorExpression",
                        "token_count": 3,
                        "operator": "<",
                        "lhs": {
                            "type": "VariableExpression",
                            "token_count": 1,
                            "token": "foo"
                        },
                        "rhs": {
                            "type": "VariableExpression",
                            "token_count": 1,
                            "token": "bar"
                        }
                    }
                }
                JSON
            ],
            'nested parenthesis' => [
                '(foo >= (1 + 2) * 3)', <<<JSON
                {
                    "type": "ParenthesisExpression",
                    "token_count": 11,
                    "expression": {
                        "type": "OperatorExpression",
                        "token_count": 9,
                        "operator": ">=",
                        "lhs": {
                            "type": "VariableExpression",
                            "token_count": 1,
                            "token": "foo"
                        },
                        "rhs": {
                            "type": "OperatorExpression",
                            "token_count": 7,
                            "operator": "*",
                            "lhs": {
                                "type": "ParenthesisExpression",
                                "token_count": 5,
                                "expression": {
                                    "type": "OperatorExpression",
                                    "token_count": 3,
                                    "operator": "+",
                                    "lhs": {
                                        "type": "LiteralExpression",
                                        "token_count": 1,
                                        "token": "1"
                                    },
                                    "rhs": {
                                        "type": "LiteralExpression",
                                        "token_count": 1,
                                        "token": "2"
                                    }
                                }
                            },
                            "rhs": {
                                "type": "LiteralExpression",
                                "token_count": 1,
                                "token": "3"
                            }
                        }
                    }
                }
                JSON,
            ]
        ];
    }
}