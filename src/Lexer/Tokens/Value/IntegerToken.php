<?php declare(strict_types=1);

namespace judahnator\Lua\Lexer\Tokens\Value;

use JetBrains\PhpStorm\Pure;
use Judahnator\Lexer\Contract\BufferInterface;
use Judahnator\Lexer\TokenMismatchException;
use judahnator\Lua\Lexer\Keywords;
use judahnator\Lua\Types\NumberType;
use judahnator\Lua\Types\Type;

final class IntegerToken implements Token
{
    private string $literal = '';

    /**
     * Token constructor.
     * If the token matches the buffer then fills this instance, otherwise throws token exception.
     *
     * @param BufferInterface $characters
     * @throws TokenMismatchException
     */
    public function __construct(BufferInterface $characters)
    {
        $offset = 0;
        while (true) {
            $char = $characters->readOne($offset++)->getLiteral();
            if (ctype_digit($char) || ($char === '-' && $this->literal === '')) {
                $this->literal .= $char;
            } else {
                break;
            }
        }
        if ($this->literal === '') {
            throw new TokenMismatchException("Integers may not be empty.");
        }
        if ($this->literal === '-') {
            throw new TokenMismatchException("Integers may not contain only \"-\".");
        }
    }

    /**
     * Provides the token length.
     *
     * @return int
     */
    #[Pure] public function count(): int
    {
        return strlen($this->literal);
    }

    /**
     * Returns the literal value of the token.
     *
     * @return string
     */
    public function getLiteral(): string
    {
        return $this->literal;
    }

    public function getName(): string
    {
        return Keywords::INT->name;
    }

    /**
     * Returns the "type" representation of this tokens value.
     * @return Type
     */
    #[Pure] public function getValue(): Type
    {
        return new NumberType((int)$this->literal);
    }

    public function isWhitespace(): bool
    {
        return false;
    }
}