<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\Types;

use judahnator\Lua\Types\NumberType;

/**
 * Class NumberTypeTest
 * @package judahnator\Lua\Tests\Types
 * @covers \judahnator\Lua\Types\NumberType
 */
final class NumberTypeTest extends TypeTestCase
{
    public function provideBooleanData(): array
    {
        return [
            'zero is true' => [
                new NumberType(0), true,
            ],
            'floats are true' => [
                new NumberType(1.0), true,
            ],
            'negative numbers are true' => [
                new NumberType(-1), true,
            ],
            'positive ints are true' => [
                new NumberType(2), true,
            ],
        ];
    }

    public function provideStringableData(): array
    {
        return [
            'zero is zero' => ['0', new NumberType(0)],
            'floats are floaty' => ['1.23', new NumberType(1.23)],
            'negative numbers have minus sign' => ['-42', new NumberType(-42)],
        ];
    }

    public function provideValueData(): array
    {
        return [
            'zero is zero' => [
                new NumberType(0), 0,
            ],
            'floats get floats' => [
                new NumberType(1.0), 1.0,
            ],
            'negative numbers work' => [
                new NumberType(-2), -2,
            ],
        ];
    }
}