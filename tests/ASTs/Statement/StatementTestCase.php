<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\ASTs\Statement;

use judahnator\Lua\AST\Compiler;
use judahnator\Lua\AST\Statement\Statement;
use Judahnator\Lexer\Buffer\CharacterBuffer as Characters;;
use judahnator\Lua\Environment;
use judahnator\Lua\STDIO;
use judahnator\Lua\Tests\ASTs\AbstractSyntaxTreeTestCase;
use judahnator\Lua\Tests\ASTs\PrefixTest;

abstract class StatementTestCase extends AbstractSyntaxTreeTestCase
{
    use PrefixTest;

    abstract public function provideParseData(): array;

    /**
     * @dataProvider provideParseData
     * @param string $input
     * @param string $expectedAST
     */
    final public function testParseFunction(string $input, string $expectedAST): void
    {
        $statement = Compiler::compileString($input)->current();
        if (!$statement instanceof Statement) {
            $this->fail('The provided input string does not represent a statement.');
        }
        $this->assertEquals($expectedAST, json_encode($statement->parse(new Environment(IO: new STDIO())), JSON_PRETTY_PRINT));
    }
}