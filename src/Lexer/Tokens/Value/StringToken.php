<?php declare(strict_types=1);

namespace judahnator\Lua\Lexer\Tokens\Value;

use JetBrains\PhpStorm\Pure;
use Judahnator\Lexer\Contract\BufferInterface;
use Judahnator\Lexer\TokenMismatchException;
use judahnator\Lua\Lexer\Keywords;
use judahnator\Lua\Types\StringType;
use judahnator\Lua\Types\Type;

final class StringToken implements Token
{
    private string $literal;

    /**
     * Token constructor.
     * If the token matches the buffer then fills this instance, otherwise throws token exception.
     *
     * @param BufferInterface $characters
     * @throws TokenMismatchException
     */
    public function __construct(BufferInterface $characters)
    {
        $firstChar = $characters->readOne()->getLiteral();
        [$single, $double] = [$firstChar === '\'', $firstChar === '"'];
        if (!$single && !$double) {
            throw new TokenMismatchException('Strings must start with a single or double quote.');
        }

        $this->literal = $firstChar;
        $offset = 1;
        do {
            $this->literal .= $characters->readOne($offset)->getLiteral();
            $offset++;
        } while (substr($this->literal, -1) !== $firstChar);
    }

    /**
     * Provides the token length.
     *
     * @return int
     */
    #[Pure] public function count(): int
    {
        return strlen($this->literal);
    }

    /**
     * Returns the literal value of the token.
     *
     * @return string
     */
    public function getLiteral(): string
    {
        return $this->literal;
    }

    public function getName(): string
    {
        return Keywords::STRING->name;
    }

    /**
     * Returns the "type" representation of this tokens value.
     * @return Type
     */
    public function getValue(): Type
    {
        return new StringType(trim($this->literal, '\'"'));
    }

    public function isWhitespace(): bool
    {
        return false;
    }
}