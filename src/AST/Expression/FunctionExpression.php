<?php

namespace judahnator\Lua\AST\Expression;

use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;
use judahnator\Lua\Environment;
use judahnator\Lua\Types\LambdaType;
use judahnator\Lua\Types\Type;

final class FunctionExpression extends Expression
{
    public Environment $scope;

    public function __construct(public LambdaType $lambda) {}

    #[ArrayShape([
        'name' => "null|string",
        'args' => "array",
        'body' => "array",
    ])]
    public function __debugInfo(): array
    {
        return [
            'name' => $this->lambda->name,
            'args' => $this->lambda->args,
            'body' => $this->lambda->body,
        ];
    }

    public function __toString(): string
    {
        return (string)$this->lambda;
    }

    /**
     * Returns the number of tokens involved in this AST node.
     * @return int
     */
    public function count(): int
    {
        // 4 for {function} {parens} {end}
        // one for the name if named
        // A reduction of the count of the body
        // Zero if no args, otherwise one less than twice the args count (because one less comma than args)
        return
            4 +
            (is_null($this->lambda->name) ? 0 : 1) +
            self::countList(...$this->lambda->body) +
            count($this->lambda->args);
    }

    public function getResult(Environment $variables): Type
    {
        return $this->lambda;
    }
}
