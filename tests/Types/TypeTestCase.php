<?php declare(strict_types=1);

namespace judahnator\Lua\Tests\Types;

use judahnator\Lua\Types\Type;
use PHPUnit\Framework\TestCase;

abstract class TypeTestCase extends TestCase
{
    /**
     * Provides values that we can test if true or false.
     *
     * @return array
     */
    abstract public function provideBooleanData(): array;

    /**
     * Provides values to check against strings.
     *
     * @return array
     */
    abstract public function provideStringableData(): array;

    /**
     * Provides data where we can test for expected data.
     *
     * @return array
     */
    abstract public function provideValueData(): array;

    /**
     * Tests that the proper input values can be retrieved.
     *
     * @dataProvider provideValueData
     * @param Type $actual
     * @param mixed $expected
     */
    public function testGettingValue(Type $actual, $expected): void
    {
        $this->assertEquals($actual->getValue(), $expected);
    }

    /**
     * Tests that we can appropriately cast to a boolean.
     *
     * @dataProvider provideBooleanData
     * @param Type $actual
     * @param bool $expected
     */
    public function testToBoolFunction(Type $actual, bool $expected): void
    {
        $this->assertSame($expected, $actual->toBool());
    }

    /**
     * Tests that each type can represent itself.
     *
     * @dataProvider provideStringableData
     */
    public function testToStringFunction(string $expected, Type $actual): void
    {
        $this->assertEquals($expected, (string)$actual);
    }
}