<?php declare(strict_types=1);

namespace judahnator\Lua\AST\Statement;

use JetBrains\PhpStorm\Immutable;
use Judahnator\Lexer\Buffer\TokenBuffer;
use judahnator\Lua\AST\AST;
use judahnator\Lua\AST\Compiler;
use judahnator\Lua\AST\Expression\Expression;
use judahnator\Lua\Environment;
use judahnator\Lua\Exceptions\AST\MismatchException;
use judahnator\Lua\Lexer\Keywords;
use judahnator\Lua\Lexer\Tokens as Token;

#[Immutable]
final class WhileLoop extends Statement
{
    public function __construct(
        private Expression $condition,

        /** @var AST[] $loop */
        private array $loop,
    ) {}

    public function __debugInfo(): array
    {
        return [
            'condition' => $this->condition,
            'loop' => $this->loop,
        ];
    }

    public function __toString(): string
    {
        return sprintf('while %s do %s end', $this->condition, implode(' ', $this->loop));
    }

    /**
     * Returns the number of tokens involved in this AST node.
     * @return int
     */
    public function count(): int
    {
        return 3 +
            count($this->condition) +
            self::countList(...$this->loop);
    }

    /**
     * Returns the AST nodes that must be parsed as a part of this statement.
     *
     * @param Environment $variables
     * @return iterable<AST>
     */
    public function parse(Environment $variables): iterable
    {
        while ($this->condition->getResult($variables)->toBool()) {
            yield from $this->loop;
        }
    }
}
